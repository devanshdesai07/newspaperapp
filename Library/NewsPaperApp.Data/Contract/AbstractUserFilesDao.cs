﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Data.Contract
{
    public abstract class AbstractUserFilesDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractUserFiles> UserFiles_Upsert(AbstractUserFiles abstractUserFiles);
        public abstract SuccessResult<AbstractUserFiles> UserFiles_Delete(long Id);
        public abstract SuccessResult<AbstractUserFiles> UserFiles_ById(long Id);
        
        public abstract PagedList<AbstractUserFiles> UserFiles_ByUserId(PageParam pageParam, string search, AbstractUserFiles abstractUserFiles);

    }
}
