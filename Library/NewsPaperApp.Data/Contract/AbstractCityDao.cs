﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Data.Contract
{
    public abstract class AbstractCityDao : AbstractBaseDao
    {
        public abstract PagedList<AbstractCity> City_ByStateId(PageParam pageParam,string CityName);

    }
}

