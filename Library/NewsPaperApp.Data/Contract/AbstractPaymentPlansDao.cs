﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Data.Contract
{
    public abstract class AbstractPaymentPlansDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractPaymentPlans> PaymentPlans_Upsert(AbstractPaymentPlans abstractPaymentPlans);
        public abstract PagedList<AbstractPaymentPlans> PaymentPlans_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractPaymentPlans> PaymentPlans_ById(long Id);
    }
} 
