﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Data.Contract
{
    public abstract class AbstractUsersDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers);
        public abstract SuccessResult<AbstractUsers> Free_Users_Insert(AbstractUsers abstractUsers);
        public abstract PagedList<AbstractUsers> Users_All(PageParam pageParam, string search, string Name, string Email, string PhoneNumber, long PaymentPlanId, string PaymentPlanExpiryDateFrom, string PaymentPlanExpiryDateTo, int IsActive, int FreeUser, string OsType);
        public abstract SuccessResult<AbstractUsers> Users_ById(long Id,string DeviceUDId);
        public abstract SuccessResult<AbstractUsers> Users_ActInact(long Id, int TrialDays, string ActivationNote, long UpdatedBy);
        public abstract SuccessResult<AbstractUsers> Users_Login(string Email, string Password);
        public abstract SuccessResult<AbstractUsers> Users_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword);
        public abstract bool Users_Logout(long Id);
        public abstract SuccessResult<AbstractUsers> Users_SendOTP(long PhoneNumber, string DeviceToken, string DeviceUDId, long OTP, string OS);
        public abstract SuccessResult<AbstractUsers> Users_VerifyOTP(string PhoneNumber, long Otp);
        public abstract SuccessResult<AbstractUsers> UserLanguage_Select(long UserId, long LanguageId);
        public abstract SuccessResult<AbstractUsers> Users_UpdatePaymentDate(long Id, DateTime PaymentDate);
        public abstract SuccessResult<AbstractUsers> Users_Force_Logout(string PhoneNumber);
        public abstract SuccessResult<AbstractUsers> Users_Delete(long Id);
        public abstract SuccessResult<AbstractUsers> Free_Users_Update(long Id);
        public abstract PagedList<AbstractLanguages> Languages_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractUsers> Users_BulkEdit(string UserIds, int Type, int TrialDays, long UpdatedBy);
    }
} 
