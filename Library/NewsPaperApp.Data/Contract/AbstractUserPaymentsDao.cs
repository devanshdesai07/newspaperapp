﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Data.Contract
{
    public abstract class AbstractUserPaymentsDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractUserPayments> UserPayments_Upsert(AbstractUserPayments abstractUserPayments);
        public abstract PagedList<AbstractUserPayments> UserPayments_ByUserId(PageParam pageParam, long UserId);
        
    }
} 
