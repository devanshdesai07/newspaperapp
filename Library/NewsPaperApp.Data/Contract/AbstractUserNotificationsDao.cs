﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Data.Contract
{
    public abstract class AbstractUserNotificationsDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractUserNotifications> UserNotifications_Upsert(AbstractUserNotifications abstractUserNotifications);
        public abstract PagedList<AbstractUserNotifications> UserNotifications_All(PageParam pageParam, string search, long UserId, string FromDate, string ToDate, long IsRead);
        public abstract SuccessResult<AbstractUserNotifications> UserNotifications_ById(long Id);
        public abstract PagedList<AbstractUserNotifications> UserNotifications_ByUserId(PageParam pageParam, long UserId);
        public abstract SuccessResult<AbstractUserNotifications> UserNotifications_ReadUnRead(long UserId);
        public abstract PagedList<AbstractUserNotifications> UserNotifications_ByUserIdReadUnRead(PageParam pageParam, long UserId);

    }
}
