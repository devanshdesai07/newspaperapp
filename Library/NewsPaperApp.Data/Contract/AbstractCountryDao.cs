﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Data.Contract
{
    public abstract class AbstractCountryDao : AbstractBaseDao
    {
        public abstract PagedList<AbstractCountry> Country_All(PageParam pageParam, string search);

    }
}
