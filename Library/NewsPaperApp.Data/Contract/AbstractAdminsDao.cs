﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Data.Contract
{
    public abstract class AbstractAdminsDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractAdmins> Admin_Upsert(AbstractAdmins abstractAdmins);
        public abstract PagedList<AbstractAdmins> Admin_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractAdmins> Admin_ById(long Id);
        public abstract SuccessResult<AbstractAdmins> Admin_ActInact(long Id);
        public abstract SuccessResult<AbstractAdmins> Admin_Login(string Username, string Password);
        public abstract SuccessResult<AbstractAdmins> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword);
        public abstract bool Admin_Logout(long Id);
        public abstract SuccessResult<AbstractAdmins> Admin_Delete(long Id);
    }
}
