﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Rushkar">
//     Copyright Rushkar Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace NewsPaperApp.Data
{
    using Autofac;
    using NewsPaperApp.Data.Contract;

    /// <summary>
    /// Contract Class for DataModule.
    /// </summary>
    public class DataModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<V1.AdminsDao>().As<AbstractAdminsDao>().InstancePerDependency();
            builder.RegisterType<V1.UsersDao>().As<AbstractUsersDao>().InstancePerDependency();
            builder.RegisterType<V1.PaymentPlansDao>().As<AbstractPaymentPlansDao>().InstancePerDependency();
            builder.RegisterType<V1.UserNotificationsDao>().As<AbstractUserNotificationsDao>().InstancePerDependency();
            builder.RegisterType<V1.UserFilesDao>().As<AbstractUserFilesDao>().InstancePerDependency();
            builder.RegisterType<V1.UploadedFilesDao>().As<AbstractUploadedFilesDao>().InstancePerDependency();
            builder.RegisterType<V1.UserPaymentsDao>().As<AbstractUserPaymentsDao>().InstancePerDependency();
            builder.RegisterType<V1.TransactionDao>().As<AbstractTransactionDao>().InstancePerDependency();
            builder.RegisterType<V1.CountryDao>().As<AbstractCountryDao>().InstancePerDependency();
            builder.RegisterType<V1.CityDao>().As<AbstractCityDao>().InstancePerDependency();
            builder.RegisterType<V1.StateDao>().As<AbstractStateDao>().InstancePerDependency();
            builder.RegisterType<V1.PaymentDao>().As<AbstractPaymentDao>().InstancePerDependency();
            base.Load(builder);
        }
    }
}
