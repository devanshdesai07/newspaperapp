﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Rushkar">
//     Copyright Rushkar. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace NewsPaperApp.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        #region Admins
        public const string Admin_Upsert = "Admin_Upsert";
        public const string Admin_All = "Admin_All";
        public const string Admin_ById = "Admin_ById";
        public const string Admin_ActInact = "Admin_ActInact";
        public const string Admin_Login = "Admin_Login";
        public const string Admin_ChangePassword = "Admin_ChangePassword";
        public const string Admin_Logout = "Admin_Logout";
        public const string Admin_Delete = "Admin_Delete";
        #endregion

        #region Users
        public const string Users_Upsert = "Users_Upsert";
        public const string Users_All = "Users_All";
        public const string Users_ById = "Users_ById";
        public const string Users_ActInact = "Users_ActInact";
        public const string Users_Login = "Users_Login";
        public const string Users_ChangePassword = "Users_ChangePassword";
        public const string Users_Logout = "Users_Logout";
        public const string Users_UpdatePaymentDate = "Users_UpdatePaymentDate";
        public const string Users_SendOTP = "Users_SendOTP";
        public const string Users_VerifyOtp = "Users_VerifyOtp";
        public const string Users_Force_Logout = "Users_Force_Logout";
        public const string Free_Users_Insert = "Free_Users_Insert";
        public const string UserLanguage_Select = "UserLanguage_Select";
        public const string Users_Delete = "Users_Delete";
        public const string Free_Users_Update = "Free_Users_Update";
        public const string Languages_All = "Languages_All";
        public const string Users_BulkEdit = "Users_BulkEdit";
        #endregion

        #region PaymentPlans
        public const string PaymentPlans_Upsert = "PaymentPlans_Upsert";
        public const string PaymentPlans_All = "PaymentPlans_All";
        public const string PaymentPlans_ById = "PaymentPlans_ById";
        #endregion

        #region UserNotifications
        public const string UserNotifications_Upsert = "UserNotifications_Upsert";
        public const string UserNotifications_All = "UserNotifications_All";
        public const string UserNotifications_ById = "UserNotifications_ById";
        public const string UserNotifications_ByUserId = "UserNotifications_ByUserId";
        public const string UserNotifications_ReadUnRead = "UserNotifications_ReadUnRead";
        public const string UserNotifications_ByUserIdReadUnRead = "UserNotifications_ByUserIdReadUnRead";
        #endregion

        #region UserFiles
        public const string UserFiles_Upsert = "UserFiles_Upsert";
        public const string UserFiles_Delete = "UserFiles_Delete";
        public const string UserFiles_ById = "UserFiles_ById";
        public const string UserFiles_ByUserId = "UserFiles_ByUserId";
        #endregion

        #region UploadedFiles
        public const string UploadedFiles_Upsert = "UploadedFiles_Upsert";
        public const string UploadedFiles_Delete = "UploadedFiles_Delete";
        public const string UploadedFiles_ById = "UploadedFiles_ById";
        public const string UploadedFiles_ByDate = "UploadedFiles_ByDate";
        public const string UploadedFiles_All = "UploadedFiles_All";
        public const string UploadedFiles_ByMonth = "UploadedFiles_ByMonth";
        public const string MasterLanguage_SelectAll = "MasterLanguage_SelectAll";
        public const string TrialSetting_ById = "TrialSetting_ById";
        public const string TrialSetting_UpdateTrialDays = "TrialSetting_UpdateTrialDays";
        #endregion

        #region UserPayments
        public const string UserPayments_Upsert = "UserPayments_Upsert";
        public const string UserPayments_ByUserId = "UserPayments_ByUserId";
        #endregion

        #region Transaction
        public const string Transaction_ByUserId = "Transaction_ByUserId";
        public const string Transaction_Insert = "Transaction_Insert";
        #endregion

        #region Country
        public const string Country_All = "Country_All";
        #endregion
        
        #region State
        public const string State_ByCountryId = "State_ByCountryId";
        #endregion
        
        #region City
        public const string City_ByStateId = "City_ByStateId";
        #endregion 
        
        #region Payment
        public const string Payment_Upsert = "Payment_Upsert";
        public const string Payment_AndroidUpsert = "Payment_AndroidUpsert";
        public const string Payment_All = "Payment_All";
        public const string Payment_ById = "Payment_ById";
        public const string Payment_ByUserId = "Payment_ByUserId";
        public const string Payment_ByTransactionId = "Payment_ByTransactionId";
        #endregion

        #region FlashScreens
        public const string FlashScreens_Insert = "FlashScreens_Insert";
        public const string FlashScreens_All = "FlashScreens_All";
        public const string FlashScreens_ById = "FlashScreens_ById";
        public const string FlashScreens_Delete = "FlashScreens_Delete";
        #endregion
    }
}
