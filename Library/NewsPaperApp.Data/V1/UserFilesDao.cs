﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class UserFilesDao : AbstractUserFilesDao
    {


        public override SuccessResult<AbstractUserFiles> UserFiles_Upsert(AbstractUserFiles abstractUserFiles)
        {
            SuccessResult<AbstractUserFiles> UserFiles = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserFiles.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserFiles.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UploadedFileId", abstractUserFiles.UploadedFileId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUserFiles.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserFiles.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserFiles_Upsert, param, commandType: CommandType.StoredProcedure);
                UserFiles = task.Read<SuccessResult<AbstractUserFiles>>().SingleOrDefault();
                UserFiles.Item = task.Read<UserFiles>().SingleOrDefault();
            }

            return UserFiles;
        }

        public override SuccessResult<AbstractUserFiles> UserFiles_ById(long Id)
        {
            SuccessResult<AbstractUserFiles> UserFiles = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserFiles_ById, param, commandType: CommandType.StoredProcedure);
                UserFiles = task.Read<SuccessResult<AbstractUserFiles>>().SingleOrDefault();
                UserFiles.Item = task.Read<UserFiles>().SingleOrDefault();
            }

            return UserFiles;
        }

        

        public override SuccessResult<AbstractUserFiles> UserFiles_Delete(long Id)
        {
            SuccessResult<AbstractUserFiles> UserFiles = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserFiles_Delete, param, commandType: CommandType.StoredProcedure);
                UserFiles = task.Read<SuccessResult<AbstractUserFiles>>().SingleOrDefault();
                UserFiles.Item = task.Read<UserFiles>().SingleOrDefault();
            }

            return UserFiles;
        }

        public override PagedList<AbstractUserFiles> UserFiles_ByUserId(PageParam pageParam, string search, AbstractUserFiles abstractUserFiles)
        {
            PagedList<AbstractUserFiles> UserFiles = new PagedList<AbstractUserFiles>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserFiles.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FileName", abstractUserFiles.FileName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FromUploadedDate", abstractUserFiles.FromUploadedDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToUploadedDate", abstractUserFiles.ToUploadedDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UploadedByUsername", abstractUserFiles.UploadedByUsername, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserFiles_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserFiles.Values.AddRange(task.Read<UserFiles>());
                UserFiles.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserFiles;
        }

    }
}
