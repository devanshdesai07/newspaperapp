﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class AdminsDao : AbstractAdminsDao
    {


        public override SuccessResult<AbstractAdmins> Admin_Upsert(AbstractAdmins abstractAdmins)
        {
            SuccessResult<AbstractAdmins> Admins = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractAdmins.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractAdmins.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractAdmins.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Username", abstractAdmins.Username, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractAdmins.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractAdmins.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractAdmins.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AdminTypeId", abstractAdmins.AdminTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_Upsert, param, commandType: CommandType.StoredProcedure);
                Admins = task.Read<SuccessResult<AbstractAdmins>>().SingleOrDefault();
                Admins.Item = task.Read<Admins>().SingleOrDefault();
            }

            return Admins;
        }

        public override PagedList<AbstractAdmins> Admin_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAdmins> Admins = new PagedList<AbstractAdmins>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_All, param, commandType: CommandType.StoredProcedure);
                Admins.Values.AddRange(task.Read<Admins>());
                Admins.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Admins;
        }

        public override SuccessResult<AbstractAdmins> Admin_ById(long Id)
        {
            SuccessResult<AbstractAdmins> Admins = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ById, param, commandType: CommandType.StoredProcedure);
                Admins = task.Read<SuccessResult<AbstractAdmins>>().SingleOrDefault();
                Admins.Item = task.Read<Admins>().SingleOrDefault();
            }

            return Admins;
        }

        public override SuccessResult<AbstractAdmins> Admin_ActInact(long Id)
        {
            SuccessResult<AbstractAdmins> Admins = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ActInact, param, commandType: CommandType.StoredProcedure);
                Admins = task.Read<SuccessResult<AbstractAdmins>>().SingleOrDefault();
                Admins.Item = task.Read<Admins>().SingleOrDefault();
            }

            return Admins;
        }

        public override SuccessResult<AbstractAdmins> Admin_Login(string Username, string Password)
        {
            SuccessResult<AbstractAdmins> Admins = null;
            var param = new DynamicParameters();

            param.Add("@Username", Username, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", Password, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_Login, param, commandType: CommandType.StoredProcedure);
                Admins = task.Read<SuccessResult<AbstractAdmins>>().SingleOrDefault();
                Admins.Item = task.Read<Admins>().SingleOrDefault();
            }

            return Admins;
        }

        public override SuccessResult<AbstractAdmins> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            SuccessResult<AbstractAdmins> Admins = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OldPassword", OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ChangePassword, param, commandType: CommandType.StoredProcedure);
                Admins = task.Read<SuccessResult<AbstractAdmins>>().SingleOrDefault();
                Admins.Item = task.Read<Admins>().SingleOrDefault();
            }

            return Admins;
        }

        public override bool Admin_Logout(long Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Admin_Logout, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }
            return result;

        }

        public override SuccessResult<AbstractAdmins> Admin_Delete(long Id)
        {
            SuccessResult<AbstractAdmins> Admins = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_Delete, param, commandType: CommandType.StoredProcedure);
                Admins = task.Read<SuccessResult<AbstractAdmins>>().SingleOrDefault();
                Admins.Item = task.Read<Admins>().SingleOrDefault();
            }

            return Admins;
        }
    }
}
