﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class UploadedFilesDao : AbstractUploadedFilesDao
    {
        public override SuccessResult<AbstractTrialSetting> TrialSetting_UpdateTrialDays(AbstractTrialSetting abstractTrialSetting)
        {
            SuccessResult<AbstractTrialSetting> TrialSetting = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTrialSetting.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TrialDays", abstractTrialSetting.TrialDays, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrialSetting_UpdateTrialDays, param, commandType: CommandType.StoredProcedure);
                TrialSetting = task.Read<SuccessResult<AbstractTrialSetting>>().SingleOrDefault();
                TrialSetting.Item = task.Read<TrialSetting>().SingleOrDefault();
            }

            return TrialSetting;
        }

        public override SuccessResult<AbstractTrialSetting> TrialSetting_ById(long Id)
        {
            SuccessResult<AbstractTrialSetting> TrialSetting = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrialSetting_ById, param, commandType: CommandType.StoredProcedure);
                TrialSetting = task.Read<SuccessResult<AbstractTrialSetting>>().SingleOrDefault();
                TrialSetting.Item = task.Read<TrialSetting>().SingleOrDefault();
            }

            return TrialSetting;
        }
        public override SuccessResult<AbstractUploadedFiles> UploadedFiles_Upsert(AbstractUploadedFiles abstractUploadedFiles)
        {
            SuccessResult<AbstractUploadedFiles> UploadedFiles = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUploadedFiles.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FileName", abstractUploadedFiles.FileName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FileUrl", abstractUploadedFiles.FileUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Note", abstractUploadedFiles.Note, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UploadedDate", abstractUploadedFiles.UploadedDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@UploadedBy", abstractUploadedFiles.UploadedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FireDate", abstractUploadedFiles.FireDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUploadedFiles.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUploadedFiles.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FileLanguageId", abstractUploadedFiles.FileLanguageId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UploadedFiles_Upsert, param, commandType: CommandType.StoredProcedure);
                UploadedFiles = task.Read<SuccessResult<AbstractUploadedFiles>>().SingleOrDefault();
                UploadedFiles.Item = task.Read<UploadedFiles>().SingleOrDefault();
            }

            return UploadedFiles;
        }

        public override PagedList<AbstractUploadedFiles> UploadedFiles_All(PageParam pageParam, string search, AbstractUploadedFiles abstractUploadedFiles)
        {
            PagedList<AbstractUploadedFiles> UploadedFiles = new PagedList<AbstractUploadedFiles>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewsPaperName", abstractUploadedFiles.NewsPaperName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FromNewsPaperDate", abstractUploadedFiles.FromNewsPaperDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToNewsPaperDate", abstractUploadedFiles.ToNewsPaperDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FromPublishedDate", abstractUploadedFiles.FromPublishedDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToPublishedDate", abstractUploadedFiles.ToPublishedDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UploadedByName", abstractUploadedFiles.UploadedByName, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FileLanguageId", abstractUploadedFiles.FileLanguageId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UploadedFiles_All, param, commandType: CommandType.StoredProcedure);
                UploadedFiles.Values.AddRange(task.Read<UploadedFiles>());
                UploadedFiles.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UploadedFiles;
        }
        public override PagedList<AbstractMasterLanguage> MasterLanguage_SelectAll(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterLanguage> MasterLanguage = new PagedList<AbstractMasterLanguage>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
           

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterLanguage_SelectAll, param, commandType: CommandType.StoredProcedure);
                MasterLanguage.Values.AddRange(task.Read<MasterLanguage>());
                MasterLanguage.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterLanguage;
        }

        public override SuccessResult<AbstractUploadedFiles> UploadedFiles_ById(long Id)
        {
            SuccessResult<AbstractUploadedFiles> UploadedFiles = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UploadedFiles_ById, param, commandType: CommandType.StoredProcedure);
                UploadedFiles = task.Read<SuccessResult<AbstractUploadedFiles>>().SingleOrDefault();
                UploadedFiles.Item = task.Read<UploadedFiles>().SingleOrDefault();
            }

            return UploadedFiles;
        }

        public override SuccessResult<AbstractUploadedFiles> UploadedFiles_ByDate(DateTime ByDate, long UserId)
        {
            SuccessResult<AbstractUploadedFiles> UserFiles = null;
            var param = new DynamicParameters();

            param.Add("@ByDate", ByDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UploadedFiles_ByDate, param, commandType: CommandType.StoredProcedure);
                UserFiles = task.Read<SuccessResult<AbstractUploadedFiles>>().SingleOrDefault();
                UserFiles.Item = task.Read<UploadedFiles>().SingleOrDefault();
            }

            return UserFiles;
        }

        public override SuccessResult<AbstractUploadedFiles> UploadedFiles_ByMonth(long ByMonth, long ByYear)
        {
            SuccessResult<AbstractUploadedFiles> UploadedFiles = null;
            var param = new DynamicParameters();

            param.Add("@ByMonth", ByMonth, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ByYear", ByYear, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UploadedFiles_ByMonth, param, commandType: CommandType.StoredProcedure);
                UploadedFiles = task.Read<SuccessResult<AbstractUploadedFiles>>().SingleOrDefault();
                UploadedFiles.Item = task.Read<UploadedFiles>().SingleOrDefault();
            }

            return UploadedFiles;
        }

        public override SuccessResult<AbstractUploadedFiles> UploadedFiles_Delete(long Id)
        {
            SuccessResult<AbstractUploadedFiles> UploadedFiles = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UploadedFiles_Delete, param, commandType: CommandType.StoredProcedure);
                UploadedFiles = task.Read<SuccessResult<AbstractUploadedFiles>>().SingleOrDefault();
                UploadedFiles.Item = task.Read<UploadedFiles>().SingleOrDefault();
            }

            return UploadedFiles;
        }

        //Flash Screen
        public override SuccessResult<AbstractFlashScreens> FlashScreens_Insert(AbstractFlashScreens abstractFlashScreens)
        {
            SuccessResult<AbstractFlashScreens> FlashScreens = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractFlashScreens.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FileUrl", abstractFlashScreens.FileUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", abstractFlashScreens.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractFlashScreens.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractFlashScreens.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.FlashScreens_Insert, param, commandType: CommandType.StoredProcedure);
                FlashScreens = task.Read<SuccessResult<AbstractFlashScreens>>().SingleOrDefault();
                FlashScreens.Item = task.Read<FlashScreens>().SingleOrDefault();
            }

            return FlashScreens;
        }

        public override PagedList<AbstractFlashScreens> FlashScreens_All(PageParam pageParam, string search, string today)
        {
            PagedList<AbstractFlashScreens> FlashScreens = new PagedList<AbstractFlashScreens>();

            var param = new DynamicParameters();
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TwoDaysOldList", today, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.FlashScreens_All, param, commandType: CommandType.StoredProcedure);
                FlashScreens.Values.AddRange(task.Read<FlashScreens>());
                FlashScreens.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return FlashScreens;
        }

        public override SuccessResult<AbstractFlashScreens> FlashScreens_ById(long Id)
        {
            SuccessResult<AbstractFlashScreens> FlashScreens = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.FlashScreens_ById, param, commandType: CommandType.StoredProcedure);
                FlashScreens = task.Read<SuccessResult<AbstractFlashScreens>>().SingleOrDefault();
                FlashScreens.Item = task.Read<FlashScreens>().SingleOrDefault();
            }

            return FlashScreens;
        }

        public override SuccessResult<AbstractFlashScreens> FlashScreens_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractFlashScreens> FlashScreens = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.FlashScreens_Delete, param, commandType: CommandType.StoredProcedure);
                FlashScreens = task.Read<SuccessResult<AbstractFlashScreens>>().SingleOrDefault();
                FlashScreens.Item = task.Read<FlashScreens>().SingleOrDefault();
            }

            return FlashScreens;
        }
    }
}
