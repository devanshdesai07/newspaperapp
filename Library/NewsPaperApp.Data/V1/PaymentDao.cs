﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class PaymentDao : AbstractPaymentDao
    {
        public override SuccessResult<AbstractPayment> Payment_Upsert(AbstractPayment abstractPayment)
        {
            SuccessResult<AbstractPayment> Payment = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPayment.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractPayment.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TransactionId", abstractPayment.TransactionId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LanguageId", abstractPayment.LanguageId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_Upsert, param, commandType: CommandType.StoredProcedure);
                Payment = task.Read<SuccessResult<AbstractPayment>>().SingleOrDefault();
                Payment.Item = task.Read<Payment>().SingleOrDefault();
            }
            return Payment;
        }

        public override SuccessResult<AbstractPayment> Payment_AndroidUpsert(AbstractPayment abstractPayment)
        {
            SuccessResult<AbstractPayment> Payment = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPayment.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractPayment.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TransactionId", abstractPayment.TransactionId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LanguageId", abstractPayment.LanguageId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_AndroidUpsert, param, commandType: CommandType.StoredProcedure);
                Payment = task.Read<SuccessResult<AbstractPayment>>().SingleOrDefault();
                Payment.Item = task.Read<Payment>().SingleOrDefault();
            }
            return Payment;
        }

        public override PagedList<AbstractPayment> Payment_All(PageParam pageParam, string search,
            long UserId, string PaymentDateFrom, string PaymentDateTo, string PaymentExpiryDateFrom,
            string PaymentExpiryDateTo)
        {
            PagedList<AbstractPayment> Payment = new PagedList<AbstractPayment>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentDateFrom", PaymentDateFrom, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaymentDateTo", PaymentDateTo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaymentExpiryDateFrom", PaymentExpiryDateFrom, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaymentExpiryDateTo", PaymentExpiryDateTo, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_All, param, commandType: CommandType.StoredProcedure);
                Payment.Values.AddRange(task.Read<Payment>());
                Payment.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Payment;
        }

        public override SuccessResult<AbstractPayment> Payment_ById(long Id)
        {
            SuccessResult<AbstractPayment> Payment = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_ById, param, commandType: CommandType.StoredProcedure);
                Payment = task.Read<SuccessResult<AbstractPayment>>().SingleOrDefault();
                Payment.Item = task.Read<Payment>().SingleOrDefault();
            }

            return Payment;
        }

        public override PagedList<AbstractPayment> Payment_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractPayment> Payment = new PagedList<AbstractPayment>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_ByUserId, param, commandType: CommandType.StoredProcedure);
                Payment.Values.AddRange(task.Read<Payment>());
                Payment.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Payment;
        } 
        public override PagedList<AbstractPayment> Payment_ByTransactionId(PageParam pageParam, string TransactionId)
        {
            PagedList<AbstractPayment> Payment = new PagedList<AbstractPayment>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TransactionId", TransactionId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_ByTransactionId, param, commandType: CommandType.StoredProcedure);
                Payment.Values.AddRange(task.Read<Payment>());
                Payment.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Payment;
        }

        public override string Payment_Hash(AbstractHashPayment hashPayment)
        {
            string result = string.Empty;
            var param = new DynamicParameters();
            param.Add("@address_line_1", hashPayment.address_line_1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@address_line_2", hashPayment.address_line_2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@amount", hashPayment.amount, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@api_key", Configurations.Hashapi_key, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@city", hashPayment.city, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@country", Configurations.Hashcountry, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@currency", Configurations.Hashcurrency, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@description", hashPayment.description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@email", hashPayment.email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@mode", hashPayment.mode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@name", hashPayment.name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@order_id", hashPayment.order_id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@phone", hashPayment.phone, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@return_url", hashPayment.return_url, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@salt", Configurations.Hashsalt, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@state", hashPayment.state, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@udf1", hashPayment.udf1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@udf2", hashPayment.udf2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@udf3", hashPayment.udf3, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@udf4", hashPayment.udf4, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@udf5", hashPayment.udf5, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@zip_code", hashPayment.zip_code, dbType: DbType.String, direction: ParameterDirection.Input);

            return result;
        }
    }
}
