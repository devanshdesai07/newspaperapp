﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class PaymentPlansDao : AbstractPaymentPlansDao
    {


        public override SuccessResult<AbstractPaymentPlans> PaymentPlans_Upsert(AbstractPaymentPlans abstractPaymentPlans)
        {
            SuccessResult<AbstractPaymentPlans> PaymentPlans = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPaymentPlans.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PlanName", abstractPaymentPlans.PlanName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Price", abstractPaymentPlans.Price, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ValidForDays", abstractPaymentPlans.ValidForDays, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PaymentPlans_Upsert, param, commandType: CommandType.StoredProcedure);
                PaymentPlans = task.Read<SuccessResult<AbstractPaymentPlans>>().SingleOrDefault();
                PaymentPlans.Item = task.Read<PaymentPlans>().SingleOrDefault();
            }

            return PaymentPlans;
        }

        public override PagedList<AbstractPaymentPlans> PaymentPlans_All(PageParam pageParam, string search)
        {
            PagedList<AbstractPaymentPlans> PaymentPlans = new PagedList<AbstractPaymentPlans>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PaymentPlans_All, param, commandType: CommandType.StoredProcedure);
                PaymentPlans.Values.AddRange(task.Read<PaymentPlans>());
                PaymentPlans.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return PaymentPlans;
        }

        public override SuccessResult<AbstractPaymentPlans> PaymentPlans_ById(long Id)
        {
            SuccessResult<AbstractPaymentPlans> PaymentPlans = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PaymentPlans_ById, param, commandType: CommandType.StoredProcedure);
                PaymentPlans = task.Read<SuccessResult<AbstractPaymentPlans>>().SingleOrDefault();
                PaymentPlans.Item = task.Read<PaymentPlans>().SingleOrDefault();
            }

            return PaymentPlans;
        }

    }
}
