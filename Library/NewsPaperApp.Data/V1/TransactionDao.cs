﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class TransactionDao : AbstractTransactionDao
    {


        public override SuccessResult<AbstractTransaction> Transaction_Insert(AbstractTransaction abstractTransaction)
        {
            SuccessResult<AbstractTransaction> Transaction = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTransaction.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@User_Id", abstractTransaction.User_Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Plan_Id", abstractTransaction.Plan_Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Transaction_Id", abstractTransaction.Transaction_Id, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Transaction_Type", abstractTransaction.Transaction_Type, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Transaction_Status", abstractTransaction.Transaction_Status, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Transaction_Insert, param, commandType: CommandType.StoredProcedure);
                Transaction = task.Read<SuccessResult<AbstractTransaction>>().SingleOrDefault();
                Transaction.Item = task.Read<Transaction>().SingleOrDefault();
            }

            return Transaction;
        }

        public override PagedList<AbstractTransaction> Transaction_ByUserId(PageParam pageParam, string search, long User_Id, long Plan_Id, long Transaction_Status, string FromCreatedDate, string ToCreatedDate)
        {
            PagedList<AbstractTransaction> Transaction = new PagedList<AbstractTransaction>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@User_Id", User_Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Plan_Id", Plan_Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Transaction_Status", Transaction_Status, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FromCreatedDate", FromCreatedDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToCreatedDate", ToCreatedDate, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Transaction_ByUserId, param, commandType: CommandType.StoredProcedure);
                Transaction.Values.AddRange(task.Read<Transaction>());
                Transaction.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Transaction;
        }

    }
}
