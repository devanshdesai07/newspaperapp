﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class StateDao : AbstractStateDao
    {


        
        public override PagedList<AbstractState> State_ByCountryId(PageParam pageParam, long CountryId)
        {
            PagedList<AbstractState> State = new PagedList<AbstractState>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CountryId", CountryId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.State_ByCountryId, param, commandType: CommandType.StoredProcedure);
                State.Values.AddRange(task.Read<State>());
                State.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return State;
        }

       

    }
}
