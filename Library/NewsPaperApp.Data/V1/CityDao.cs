﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class CityDao : AbstractCityDao
    {



        public override PagedList<AbstractCity> City_ByStateId(PageParam pageParam, string CityName)
        {
            PagedList<AbstractCity> City = new PagedList<AbstractCity>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CityName", CityName, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.City_ByStateId, param, commandType: CommandType.StoredProcedure);
                City.Values.AddRange(task.Read<City>());
                City.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return City;
        }



    }
}
