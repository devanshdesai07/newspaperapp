﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class CountryDao : AbstractCountryDao
    {
        public override PagedList<AbstractCountry> Country_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCountry> Country = new PagedList<AbstractCountry>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Country_All, param, commandType: CommandType.StoredProcedure);
                Country.Values.AddRange(task.Read<Country>());
                Country.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Country;
        }
    }
}
