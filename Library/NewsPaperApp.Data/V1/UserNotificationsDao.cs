﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class UserNotificationsDao : AbstractUserNotificationsDao
    {


        public override SuccessResult<AbstractUserNotifications> UserNotifications_Upsert(AbstractUserNotifications abstractUserNotifications)
        {
            try
            {
                SuccessResult<AbstractUserNotifications> UserNotifications = null;
                var param = new DynamicParameters();

                param.Add("@Id", abstractUserNotifications.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UserIds", abstractUserNotifications.UserIds, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@NotificationText", abstractUserNotifications.NotificationText, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractUserNotifications.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.UserNotifications_Upsert, param, commandType: CommandType.StoredProcedure);
                    UserNotifications = task.Read<SuccessResult<AbstractUserNotifications>>().SingleOrDefault();
                    UserNotifications.Item = task.Read<UserNotifications>().SingleOrDefault();
                }

                return UserNotifications;
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }

        public override PagedList<AbstractUserNotifications> UserNotifications_All(PageParam pageParam, string search, long UserId, string FromDate, string ToDate, long IsRead)
        {
            PagedList<AbstractUserNotifications> UserNotifications = new PagedList<AbstractUserNotifications>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FromDate", FromDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToDate", ToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsRead", IsRead, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_All, param, commandType: CommandType.StoredProcedure);
                UserNotifications.Values.AddRange(task.Read<UserNotifications>());
                UserNotifications.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserNotifications;
        }

        public override SuccessResult<AbstractUserNotifications> UserNotifications_ById(long Id)
        {
            SuccessResult<AbstractUserNotifications> UserNotifications = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_ById, param, commandType: CommandType.StoredProcedure);
                UserNotifications = task.Read<SuccessResult<AbstractUserNotifications>>().SingleOrDefault();
                UserNotifications.Item = task.Read<UserNotifications>().SingleOrDefault();
            }

            return UserNotifications;
        }
        public override SuccessResult<AbstractUserNotifications> UserNotifications_ReadUnRead(long Id)
        {
            SuccessResult<AbstractUserNotifications> UserNotifications = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_ReadUnRead, param, commandType: CommandType.StoredProcedure);
                UserNotifications = task.Read<SuccessResult<AbstractUserNotifications>>().SingleOrDefault();
                UserNotifications.Item = task.Read<UserNotifications>().SingleOrDefault();
            }

            return UserNotifications;
        }
        public override PagedList<AbstractUserNotifications> UserNotifications_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractUserNotifications> UserNotifications = new PagedList<AbstractUserNotifications>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserNotifications.Values.AddRange(task.Read<UserNotifications>());
                UserNotifications.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserNotifications;
        }

        public override PagedList<AbstractUserNotifications> UserNotifications_ByUserIdReadUnRead(PageParam pageParam, long UserId)
        {
            PagedList<AbstractUserNotifications> UserNotifications = new PagedList<AbstractUserNotifications>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotifications_ByUserIdReadUnRead, param, commandType: CommandType.StoredProcedure);
                UserNotifications.Values.AddRange(task.Read<UserNotifications>());
                UserNotifications.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserNotifications;
        }

    }
}
