﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class UserPaymentsDao : AbstractUserPaymentsDao
    {


        public override SuccessResult<AbstractUserPayments> UserPayments_Upsert(AbstractUserPayments abstractUserPayments)
        {
            try
            {
                SuccessResult<AbstractUserPayments> UserPayments = null;
                var param = new DynamicParameters();

                param.Add("@Id", abstractUserPayments.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UserId", abstractUserPayments.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@PaymentDate", abstractUserPayments.PaymentDate, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@TransactionNumber", abstractUserPayments.TransactionNumber, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@MerchantNumber", abstractUserPayments.MerchantNumber, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@PlanId", abstractUserPayments.PlanId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@PlanExpiryDate", abstractUserPayments.PlanExpiryDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractUserPayments.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractUserPayments.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.UserPayments_Upsert, param, commandType: CommandType.StoredProcedure);
                    UserPayments = task.Read<SuccessResult<AbstractUserPayments>>().SingleOrDefault();
                    UserPayments.Item = task.Read<UserPayments>().SingleOrDefault();
                }

                return UserPayments;
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }

        public override PagedList<AbstractUserPayments> UserPayments_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractUserPayments> UserPayments = new PagedList<AbstractUserPayments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserPayments_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserPayments.Values.AddRange(task.Read<UserPayments>());
                UserPayments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserPayments;
        }

    }
}
