﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Entities.V1;
using Dapper;

namespace NewsPaperApp.Data.V1
{
    public class UsersDao : AbstractUsersDao
    {
        public override SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUsers.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractUsers.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractUsers.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractUsers.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractUsers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaymentPlanId", abstractUsers.PaymentPlanId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractUsers.CountryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StateId", abstractUsers.StateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CityId", abstractUsers.CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LanguageId", abstractUsers.LanguageId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Upsert, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Free_Users_Insert(AbstractUsers abstractUsers)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@FirstName", abstractUsers.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractUsers.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", abstractUsers.PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractUsers.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractUsers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractUsers.CountryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StateId", abstractUsers.StateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CityId", abstractUsers.CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentExpiryDate", abstractUsers.PaymentExpiryDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@OS", abstractUsers.OS, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Free_Users_Insert, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override PagedList<AbstractUsers> Users_All(PageParam pageParam, string search, string Name, string Email, 
            string PhoneNumber, long PaymentPlanId, string PaymentPlanExpiryDateFrom, string PaymentPlanExpiryDateTo, 
            int IsActive, int FreeUser, string OsType)
        {
            PagedList<AbstractUsers> Users = new PagedList<AbstractUsers>();

            var param = new DynamicParameters();
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name ", Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Phonenumber", PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaymentPlanId", PaymentPlanId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentPlanExpiryDateFrom", PaymentPlanExpiryDateFrom, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaymentPlanExpiryDateTo", PaymentPlanExpiryDateTo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", IsActive, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FreeUser", FreeUser, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@OsType", OsType, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_All, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<Users>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_ById(long Id,string DeviceUDId)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeviceUDId", DeviceUDId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ById, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        } 
        public override SuccessResult<AbstractUsers> UserLanguage_Select(long UserId,long LanguageId)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LanguageId", LanguageId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserLanguage_Select, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractUsers> Users_ActInact(long Id, int TrialDays, string ActivationNote, long UpdatedBy)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TrialDays", TrialDays, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ActivationNote", ActivationNote, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ActInact, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_Login(string Email, string Password)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", Password, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Login, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OldPassword", OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ChangePassword, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override bool Users_Logout(long Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Users_Logout, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }
            return result;
        }

        public override SuccessResult<AbstractUsers> Users_SendOTP(long PhoneNumber, string DeviceToken, string DeviceUDId, long OTP, string OS)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@PhoneNumber", PhoneNumber, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeviceUDId", DeviceUDId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OTP", OTP, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OS", OS, dbType: DbType.String, direction: ParameterDirection.Input);
           

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_SendOTP, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_VerifyOTP(string PhoneNumber, long Otp)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@PhoneNumber", PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Otp", Otp, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_VerifyOtp, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_UpdatePaymentDate(long Id, DateTime PaymentDate)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentDate", PaymentDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_UpdatePaymentDate, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractUsers> Users_Force_Logout(string PhoneNumber)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@PhoneNumber", PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Force_Logout, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_Delete(long Id)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Delete, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUsers> Free_Users_Update(long Id)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Free_Users_Update, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }

        public override PagedList<AbstractLanguages> Languages_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLanguages> Users = new PagedList<AbstractLanguages>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Languages_All, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<Languages>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_BulkEdit(string UserIds, int Type, int TrialDays, long UpdatedBy)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@UserIds", UserIds, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TrialDays", TrialDays, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_BulkEdit, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
    }
 }
