﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace NewsPaperApp.Services
{
    using Autofac;
    using Data;
    using NewsPaperApp.Services.Contract;
    



    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DataModule>();

            builder.RegisterType<V1.AdminsServices>().As<AbstractAdminsServices>().InstancePerDependency();
            builder.RegisterType<V1.UsersServices>().As<AbstractUsersServices>().InstancePerDependency();
            builder.RegisterType<V1.PaymentPlansServices>().As<AbstractPaymentPlansServices>().InstancePerDependency();
            builder.RegisterType<V1.UserNotificationsServices>().As<AbstractUserNotificationsServices>().InstancePerDependency();
            builder.RegisterType<V1.UserFilesServices>().As<AbstractUserFilesServices>().InstancePerDependency();
            builder.RegisterType<V1.UploadedFilesServices>().As<AbstractUploadedFilesServices>().InstancePerDependency();
            builder.RegisterType<V1.UserPaymentsServices>().As<AbstractUserPaymentsServices>().InstancePerDependency();
            builder.RegisterType<V1.CountryServices>().As<AbstractCountryServices>().InstancePerDependency();
            builder.RegisterType<V1.StateServices>().As<AbstractStateServices>().InstancePerDependency();
            builder.RegisterType<V1.CityServices>().As<AbstractCityServices>().InstancePerDependency();
            builder.RegisterType<V1.PaymentServices>().As<AbstractPaymentServices>().InstancePerDependency();
            builder.RegisterType<V1.TransactionServices>().As<AbstractTransactionServices>().InstancePerDependency();
            




            base.Load(builder);
        }
    }
}
