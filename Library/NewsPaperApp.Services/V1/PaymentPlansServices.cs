﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class PaymentPlansServices : AbstractPaymentPlansServices
    {
        private AbstractPaymentPlansDao abstractPaymentPlansDao;

        public PaymentPlansServices(AbstractPaymentPlansDao abstractPaymentPlansDao)
        {
            this.abstractPaymentPlansDao = abstractPaymentPlansDao;
        }


        public override SuccessResult<AbstractPaymentPlans> PaymentPlans_Upsert(AbstractPaymentPlans abstractPaymentPlans)
        {
            return this.abstractPaymentPlansDao.PaymentPlans_Upsert(abstractPaymentPlans); ;
        }

        public override PagedList<AbstractPaymentPlans> PaymentPlans_All(PageParam pageParam, string search)
        {
            return this.abstractPaymentPlansDao.PaymentPlans_All(pageParam, search);
        }

        public override SuccessResult<AbstractPaymentPlans> PaymentPlans_ById(long Id)
        {
            return this.abstractPaymentPlansDao.PaymentPlans_ById(Id);
        }
        
    }

}