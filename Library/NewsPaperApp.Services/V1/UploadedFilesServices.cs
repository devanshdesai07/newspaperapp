﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class UploadedFilesServices : AbstractUploadedFilesServices
    {
        private AbstractUploadedFilesDao abstractUploadedFilesDao;

        public UploadedFilesServices(AbstractUploadedFilesDao abstractUploadedFilesDao)
        {
            this.abstractUploadedFilesDao = abstractUploadedFilesDao;
        }


        public override SuccessResult<AbstractUploadedFiles> UploadedFiles_Upsert(AbstractUploadedFiles abstractUploadedFiles)
        {
            return this.abstractUploadedFilesDao.UploadedFiles_Upsert(abstractUploadedFiles);
        }

        public override PagedList<AbstractUploadedFiles> UploadedFiles_All(PageParam pageParam, string search, AbstractUploadedFiles abstractUploadedFiles)
        {
            return this.abstractUploadedFilesDao.UploadedFiles_All(pageParam, search, abstractUploadedFiles);
        }
        public override PagedList<AbstractMasterLanguage> MasterLanguage_SelectAll(PageParam pageParam, string search)
        {
            return this.abstractUploadedFilesDao.MasterLanguage_SelectAll(pageParam, search);
        }

        public override SuccessResult<AbstractUploadedFiles> UploadedFiles_ById(long Id)
        {
            return this.abstractUploadedFilesDao.UploadedFiles_ById(Id);
        }

        public override SuccessResult<AbstractUploadedFiles> UploadedFiles_ByDate(DateTime ByDate, long UserId)
        {
            return this.abstractUploadedFilesDao.UploadedFiles_ByDate(ByDate, UserId);
        }

        public override SuccessResult<AbstractUploadedFiles> UploadedFiles_ByMonth(long ByMonth, long ByYear)
        {
            return this.abstractUploadedFilesDao.UploadedFiles_ByMonth(ByMonth, ByYear);
        }

        public override SuccessResult<AbstractUploadedFiles> UploadedFiles_Delete(long Id)
        {
            return this.abstractUploadedFilesDao.UploadedFiles_Delete(Id);
        }
        public override SuccessResult<AbstractTrialSetting> TrialSetting_ById(long Id)
        {
            return this.abstractUploadedFilesDao.TrialSetting_ById(Id);
        }
        public override SuccessResult<AbstractTrialSetting> TrialSetting_UpdateTrialDays(AbstractTrialSetting abstractTrialSetting)
        {
            return this.abstractUploadedFilesDao.TrialSetting_UpdateTrialDays(abstractTrialSetting);
        }

        //Flash Screen
        public override SuccessResult<AbstractFlashScreens> FlashScreens_Insert(AbstractFlashScreens abstractFlashScreens)
        {
            return this.abstractUploadedFilesDao.FlashScreens_Insert(abstractFlashScreens);
        }
        public override PagedList<AbstractFlashScreens> FlashScreens_All(PageParam pageParam, string search, string today)
        {
            return this.abstractUploadedFilesDao.FlashScreens_All(pageParam, search, today);
        }
        public override SuccessResult<AbstractFlashScreens> FlashScreens_ById(long Id)
        {
            return this.abstractUploadedFilesDao.FlashScreens_ById(Id);
        }
        public override SuccessResult<AbstractFlashScreens> FlashScreens_Delete(long Id, long DeletedBy)
        {
            return this.abstractUploadedFilesDao.FlashScreens_Delete(Id, DeletedBy);
        }
    }

}