﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class UserFilesServices : AbstractUserFilesServices
    {
        private AbstractUserFilesDao abstractUserFilesDao;

        public UserFilesServices(AbstractUserFilesDao abstractUserFilesDao)
        {
            this.abstractUserFilesDao = abstractUserFilesDao;
        }


        public override SuccessResult<AbstractUserFiles> UserFiles_Upsert(AbstractUserFiles abstractUserFiles)
        {
            return this.abstractUserFilesDao.UserFiles_Upsert(abstractUserFiles); ;
        }

        public override SuccessResult<AbstractUserFiles> UserFiles_Delete(long Id)
        {
            return this.abstractUserFilesDao.UserFiles_Delete(Id);
        }

        public override SuccessResult<AbstractUserFiles> UserFiles_ById(long Id)
        {
            return this.abstractUserFilesDao.UserFiles_ById(Id);
        }

        

        public override PagedList<AbstractUserFiles> UserFiles_ByUserId(PageParam pageParam, string search, AbstractUserFiles abstractUserFiles)
        {
            return this.abstractUserFilesDao.UserFiles_ByUserId(pageParam, search, abstractUserFiles);
        }

    }

}