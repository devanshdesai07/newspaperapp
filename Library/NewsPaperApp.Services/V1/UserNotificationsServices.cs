﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class UserNotificationsServices : AbstractUserNotificationsServices
    {
        private AbstractUserNotificationsDao abstractUserNotificationsDao;

        public UserNotificationsServices(AbstractUserNotificationsDao abstractUserNotificationsDao)
        {
            this.abstractUserNotificationsDao = abstractUserNotificationsDao;
        }


        public override SuccessResult<AbstractUserNotifications> UserNotifications_Upsert(AbstractUserNotifications abstractUserNotifications)
        {
            return this.abstractUserNotificationsDao.UserNotifications_Upsert(abstractUserNotifications);
        }

        public override PagedList<AbstractUserNotifications> UserNotifications_All(PageParam pageParam, string search, long UserId, string FromDate, string ToDate, long IsRead)
        {
            return this.abstractUserNotificationsDao.UserNotifications_All(pageParam, search,UserId,FromDate,ToDate,IsRead);
        }

        public override SuccessResult<AbstractUserNotifications> UserNotifications_ById(long Id)
        {
            return this.abstractUserNotificationsDao.UserNotifications_ById(Id);
        }

        public override PagedList<AbstractUserNotifications> UserNotifications_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractUserNotificationsDao.UserNotifications_ByUserId(pageParam, UserId);
        }
        public override SuccessResult<AbstractUserNotifications> UserNotifications_ReadUnRead(long Id)
        {
            return this.abstractUserNotificationsDao.UserNotifications_ReadUnRead(Id);
        }

        public override PagedList<AbstractUserNotifications> UserNotifications_ByUserIdReadUnRead(PageParam pageParam, long UserId)
        {
            return this.abstractUserNotificationsDao.UserNotifications_ByUserIdReadUnRead(pageParam, UserId);
        }
    }

}