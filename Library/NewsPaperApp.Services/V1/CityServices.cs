﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class CityServices : AbstractCityServices
    {
        private AbstractCityDao abstractCityDao;

        public CityServices(AbstractCityDao abstractCityDao)
        {
            this.abstractCityDao = abstractCityDao;
        }




        public override PagedList<AbstractCity> City_ByStateId(PageParam pageParam, string CityName)
        {
            return this.abstractCityDao.City_ByStateId(pageParam, CityName);
        }

    }

}
