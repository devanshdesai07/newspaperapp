﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class UserPaymentsServices : AbstractUserPaymentsServices
    {
        private AbstractUserPaymentsDao abstractUserPaymentsDao;

        public UserPaymentsServices(AbstractUserPaymentsDao abstractUserPaymentsDao)
        {
            this.abstractUserPaymentsDao = abstractUserPaymentsDao;
        }


        public override SuccessResult<AbstractUserPayments> UserPayments_Upsert(AbstractUserPayments abstractUserPayments)
        {
            return this.abstractUserPaymentsDao.UserPayments_Upsert(abstractUserPayments); ;
        }

        public override PagedList<AbstractUserPayments> UserPayments_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractUserPaymentsDao.UserPayments_ByUserId(pageParam, UserId);
        }

    }

}