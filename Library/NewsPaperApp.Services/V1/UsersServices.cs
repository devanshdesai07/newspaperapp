﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class UsersServices : AbstractUsersServices
    {
        private AbstractUsersDao abstractUsersDao;

        public UsersServices(AbstractUsersDao abstractUsersDao)
        {
            this.abstractUsersDao = abstractUsersDao;
        }

        public override SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers)
        {
            return this.abstractUsersDao.Users_Upsert(abstractUsers); 
        }
        public override SuccessResult<AbstractUsers> Free_Users_Insert(AbstractUsers abstractUsers)
        {
            return this.abstractUsersDao.Free_Users_Insert(abstractUsers);
        }
        public override PagedList<AbstractUsers> Users_All(PageParam pageParam, string search, string Name, string Email, string PhoneNumber, long PaymentPlanId, string PaymentPlanExpiryDateFrom, string PaymentPlanExpiryDateTo, int IsActive, int FreeUser, string OsType)
        {
            return this.abstractUsersDao.Users_All(pageParam, search, Name, Email, PhoneNumber, PaymentPlanId, PaymentPlanExpiryDateFrom, PaymentPlanExpiryDateTo, IsActive, FreeUser, OsType);
        }
        public override SuccessResult<AbstractUsers> Users_ById(long Id,string DeviceUDId)
        {
            return this.abstractUsersDao.Users_ById(Id, DeviceUDId);
        } 
        public override SuccessResult<AbstractUsers> UserLanguage_Select(long UserId,long LanguageId)
        {
            return this.abstractUsersDao.UserLanguage_Select(UserId, LanguageId);
        }
        public override SuccessResult<AbstractUsers> Users_ActInact(long Id, int TrialDays, string ActivationNote, long UpdatedBy)
        {
            return this.abstractUsersDao.Users_ActInact(Id, TrialDays, ActivationNote, UpdatedBy);
        }
        public override SuccessResult<AbstractUsers> Users_Login(string Email, string Password)
        {
            return this.abstractUsersDao.Users_Login(Email, Password);
        }
        public override SuccessResult<AbstractUsers> Users_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            return this.abstractUsersDao.Users_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
        }
        public override bool Users_Logout(long Id)
        {
            return this.abstractUsersDao.Users_Logout(Id);
        }
        public override SuccessResult<AbstractUsers> Users_SendOTP(long PhoneNumber, string DeviceToken, string DeviceUDId, long OTP, string OS)
        {
            return this.abstractUsersDao.Users_SendOTP(PhoneNumber, DeviceToken, DeviceUDId, OTP, OS);
        }
        public override SuccessResult<AbstractUsers> Users_VerifyOTP(string PhoneNumber, long Otp)
        {
            return this.abstractUsersDao.Users_VerifyOTP(PhoneNumber, Otp);
        }
        public override SuccessResult<AbstractUsers> Users_UpdatePaymentDate(long Id, DateTime PaymentDate)
        {
            return this.abstractUsersDao.Users_UpdatePaymentDate(Id, PaymentDate);
        }
        public override SuccessResult<AbstractUsers> Users_Force_Logout(string PhoneNumber)
        {
            return this.abstractUsersDao.Users_Force_Logout(PhoneNumber);
        }
        public override SuccessResult<AbstractUsers> Users_Delete(long Id)
        {
            return this.abstractUsersDao.Users_Delete(Id);
        }
        public override SuccessResult<AbstractUsers> Free_Users_Update(long Id)
        {
            return this.abstractUsersDao.Free_Users_Update(Id);
        }
        public override PagedList<AbstractLanguages> Languages_All(PageParam pageParam, string search)
        {
            return this.abstractUsersDao.Languages_All(pageParam, search);
        }
        public override SuccessResult<AbstractUsers> Users_BulkEdit(string UserIds, int Type, int TrialDays, long UpdatedBy)
        {
            return this.abstractUsersDao.Users_BulkEdit(UserIds, Type, TrialDays, UpdatedBy);
        }
    }
}