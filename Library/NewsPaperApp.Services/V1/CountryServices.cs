﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class CountryServices : AbstractCountryServices
    {
        private AbstractCountryDao abstractCountryDao;

        public CountryServices(AbstractCountryDao abstractCountryDao)
        {
            this.abstractCountryDao = abstractCountryDao;
        }




        public override PagedList<AbstractCountry> Country_All(PageParam pageParam, string search)
        {
            return this.abstractCountryDao.Country_All(pageParam, search);
        }

    }

}