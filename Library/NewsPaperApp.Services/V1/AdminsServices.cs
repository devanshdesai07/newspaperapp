﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class AdminsServices : AbstractAdminsServices
    {
        private AbstractAdminsDao abstractAdminsDao;

        public AdminsServices(AbstractAdminsDao abstractAdminsDao)
        {
            this.abstractAdminsDao = abstractAdminsDao;
        }


        public override SuccessResult<AbstractAdmins> Admin_Upsert(AbstractAdmins abstractAdmins)
        {
            return this.abstractAdminsDao.Admin_Upsert(abstractAdmins); ;
        }

        public override PagedList<AbstractAdmins> Admin_All(PageParam pageParam, string search)
        {
            return this.abstractAdminsDao.Admin_All(pageParam, search);
        }

        public override SuccessResult<AbstractAdmins> Admin_ById(long Id)
        {
            return this.abstractAdminsDao.Admin_ById(Id);
        }
        public override SuccessResult<AbstractAdmins> Admin_ActInact(long Id)
        {
            return this.abstractAdminsDao.Admin_ActInact(Id);
        }

        public override SuccessResult<AbstractAdmins> Admin_Login(string Username, string Password)
        {
            return this.abstractAdminsDao.Admin_Login(Username, Password);
        }

        public override SuccessResult<AbstractAdmins> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            return this.abstractAdminsDao.Admin_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
        }

        public override bool Admin_Logout(long Id)
        {
            return this.abstractAdminsDao.Admin_Logout(Id);
        }

        public override SuccessResult<AbstractAdmins> Admin_Delete(long Id)
        {
            return this.abstractAdminsDao.Admin_Delete(Id);
        }

    }

}