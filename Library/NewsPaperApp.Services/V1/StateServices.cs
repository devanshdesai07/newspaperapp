﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class StateServices : AbstractStateServices
    {
        private AbstractStateDao abstractStateDao;

        public StateServices(AbstractStateDao abstractStateDao)
        {
            this.abstractStateDao = abstractStateDao;
        }


        

        public override PagedList<AbstractState> State_ByCountryId(PageParam pageParam, long CountryId)
        {
            return this.abstractStateDao.State_ByCountryId(pageParam, CountryId);
        }
       
    }

}
