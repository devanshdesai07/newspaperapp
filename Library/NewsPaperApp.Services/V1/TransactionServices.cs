﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class TransactionServices : AbstractTransactionServices
    {
        private AbstractTransactionDao abstractTransactionDao;

        public TransactionServices(AbstractTransactionDao abstractTransactionDao)
        {
            this.abstractTransactionDao = abstractTransactionDao;
        }


        public override SuccessResult<AbstractTransaction> Transaction_Insert(AbstractTransaction abstractTransaction)
        {
            return this.abstractTransactionDao.Transaction_Insert(abstractTransaction); 
        }

        public override PagedList<AbstractTransaction> Transaction_ByUserId(PageParam pageParam, string search, long User_Id, long Plan_Id, long Transaction_Status, string FromCreatedDate, string ToCreatedDate)
        {
            return this.abstractTransactionDao.Transaction_ByUserId(pageParam, search, User_Id, Plan_Id, Transaction_Status, FromCreatedDate, ToCreatedDate);
        }

    }

}