﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Data.Contract;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;

namespace NewsPaperApp.Services.V1
{
    public class PaymentServices : AbstractPaymentServices
    {
        private AbstractPaymentDao abstractPaymentDao;

        public PaymentServices(AbstractPaymentDao abstractPaymentDao)
        {
            this.abstractPaymentDao = abstractPaymentDao;
        }

        public override SuccessResult<AbstractPayment> Payment_Upsert(AbstractPayment abstractPayment)
        {
            return this.abstractPaymentDao.Payment_Upsert(abstractPayment);
        }
        public override SuccessResult<AbstractPayment> Payment_AndroidUpsert(AbstractPayment abstractPayment)
        {
            return this.abstractPaymentDao.Payment_AndroidUpsert(abstractPayment);
        }
        public override PagedList<AbstractPayment> Payment_All(PageParam pageParam, string search,
            long UserId, string PaymentDateFrom , string PaymentDateTo, string PaymentExpiryDateFrom,
            string PaymentExpiryDateTo)
        {
            return this.abstractPaymentDao.Payment_All(pageParam, search, UserId, PaymentDateFrom, PaymentDateTo,
                PaymentExpiryDateFrom, PaymentExpiryDateTo);
        }
        public override SuccessResult<AbstractPayment> Payment_ById(long Id)
        {
            return this.abstractPaymentDao.Payment_ById(Id);
        }
        public override PagedList<AbstractPayment> Payment_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractPaymentDao.Payment_ByUserId(pageParam, UserId);
        }
        public override PagedList<AbstractPayment> Payment_ByTransactionId(PageParam pageParam, string TransactionId)
        {
            return this.abstractPaymentDao.Payment_ByTransactionId(pageParam, TransactionId);
        }
        public override string Payment_Hash(AbstractHashPayment hashPayment)
        {
            return this.abstractPaymentDao.Payment_Hash(hashPayment);
        }


    }

}