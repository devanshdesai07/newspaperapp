﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Services.Contract
{
    public abstract class AbstractStateServices
    {
        public abstract PagedList<AbstractState> State_ByCountryId(PageParam pageParam, long CountryId);


    }
}


