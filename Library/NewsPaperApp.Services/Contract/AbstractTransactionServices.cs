﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Services.Contract
{
    public abstract class AbstractTransactionServices
    {
        public abstract SuccessResult<AbstractTransaction> Transaction_Insert(AbstractTransaction abstractTransaction);
        public abstract PagedList<AbstractTransaction> Transaction_ByUserId(PageParam pageParam, string search, long User_Id, long Plan_Id, long Transaction_Status, string FromCreatedDate, string ToCreatedDate);
    }
}
