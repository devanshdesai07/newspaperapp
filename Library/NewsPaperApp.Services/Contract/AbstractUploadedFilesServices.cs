﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Services.Contract
{
    public abstract class AbstractUploadedFilesServices
    {
        public abstract SuccessResult<AbstractUploadedFiles> UploadedFiles_Upsert(AbstractUploadedFiles abstractUploadedFiles);
        public abstract PagedList<AbstractUploadedFiles> UploadedFiles_All(PageParam pageParam, string search, AbstractUploadedFiles abstractUploadedFiles);
        public abstract PagedList<AbstractMasterLanguage> MasterLanguage_SelectAll(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractUploadedFiles> UploadedFiles_ById(long Id);
        public abstract SuccessResult<AbstractUploadedFiles> UploadedFiles_ByDate(DateTime ByDate, long UserId);
        public abstract SuccessResult<AbstractUploadedFiles> UploadedFiles_ByMonth(long ByMonth, long ByYear);
        public abstract SuccessResult<AbstractUploadedFiles> UploadedFiles_Delete(long Id);
        public abstract SuccessResult<AbstractTrialSetting> TrialSetting_UpdateTrialDays(AbstractTrialSetting abstractTrialSetting);
        public abstract SuccessResult<AbstractTrialSetting> TrialSetting_ById(long Id);
        public abstract PagedList<AbstractFlashScreens> FlashScreens_All(PageParam pageParam, string search, string today);
        public abstract SuccessResult<AbstractFlashScreens> FlashScreens_ById(long Id);
        public abstract SuccessResult<AbstractFlashScreens> FlashScreens_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractFlashScreens> FlashScreens_Insert(AbstractFlashScreens abstractFlashScreens);
    }
}
