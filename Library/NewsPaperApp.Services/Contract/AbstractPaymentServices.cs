﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Services.Contract
{
    public abstract class AbstractPaymentServices
    {
        public abstract SuccessResult<AbstractPayment> Payment_Upsert(AbstractPayment abstractPayment);
        public abstract SuccessResult<AbstractPayment> Payment_AndroidUpsert(AbstractPayment abstractPayment);
        public abstract PagedList<AbstractPayment> Payment_All(PageParam pageParam, string search,
            long UserId, string PaymentDateFrom, string PaymentDateTo, string PaymentExpiryDateFrom,
            string PaymentExpiryDateTo);
        public abstract SuccessResult<AbstractPayment> Payment_ById(long Id);
        public abstract PagedList<AbstractPayment> Payment_ByUserId(PageParam pageParam, long UserId);
        public abstract PagedList<AbstractPayment> Payment_ByTransactionId(PageParam pageParam, string TransactionId);
        public abstract string Payment_Hash(AbstractHashPayment hashPayment);
    }
}


