﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsPaperApp.Common;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;

namespace NewsPaperApp.Services.Contract
{
    public abstract class AbstractUserPaymentsServices
    {
        public abstract SuccessResult<AbstractUserPayments> UserPayments_Upsert(AbstractUserPayments abstractUserPayments);
        public abstract PagedList<AbstractUserPayments> UserPayments_ByUserId(PageParam pageParam, long UserId);

    }
}
