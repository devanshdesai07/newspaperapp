﻿using NewsPaperApp;
using NewsPaperApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace NewsPaperApp.Entities.Contract
{
    public abstract class AbstractUserPayments
    {
        public long Id { get; set; }
        public long UserId { get; set; }                                
        public string PaymentDate { get; set; }
        public string TransactionNumber { get; set; }
        public string MerchantNumber { get; set; }
        public bool IsPaymentDone { get; set; }
        public long PlanId { get; set; }
        public DateTime PlanExpiryDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string PlanExpiryDateStr => PlanExpiryDate != null ? PlanExpiryDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

    }
}

