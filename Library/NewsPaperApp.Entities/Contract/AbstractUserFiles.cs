﻿using NewsPaperApp;
using NewsPaperApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace NewsPaperApp.Entities.Contract
{
    public abstract class AbstractUserFiles
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserFullName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmail { get; set; }
        public string UserPhoneNumber { get; set; }
        public string UploadedFileName { get; set; }
        public string UserPaymentExpiryDate { get; set; }
        public string FromUploadedDate { get; set; }
        public string ToUploadedDate { get; set; }
        public string FileName { get; set; }
        public long UploadedFileId { get; set; }
        public string UploadedFilesFileName { get; set; }
        public string UploadedFilesFileUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public string UploadedDate { get; set; }
        public long UploadedBy { get; set; }
        public long UploadedByUsername { get; set; }
        public string AdminUserName { get; set; }
        public string FireDate { get; set; }
        

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }

}

