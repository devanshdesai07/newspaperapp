﻿using NewsPaperApp.Common;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace NewsPaperApp.Entities.Contract
{
    public abstract class AbstractUploadedFiles
    {
        public long Id { get; set; }
        public long UploadedByName { get; set; }
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public DateTime UploadedDate { get; set; }
        public long UploadedBy { get; set; }
        public DateTime FireDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public string Note { get; set; }
        public string UploadeName { get; set; }
        public string NewsPaperName { get; set; }
        public string FromNewsPaperDate { get; set; }
        public string ToNewsPaperDate { get; set; }
        public string FromPublishedDate { get; set; }
        public string ToPublishedDate { get; set; }
        public DateTime ByDate { get; set; }
        public long FileLanguageId { get; set; }
        public string FileLanguageName { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string UploadedDateStr => UploadedDate != null ? UploadedDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string FireDateStr => FireDate != null ? FireDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string ByDateStr => ByDate != null ? ByDate.ToString("dd-MMM-yyyy") : "-";
    }

    public abstract class AbstractMasterLanguage
    {
        public long Id { get; set; }
        public string Language { get; set; }
    }

    public abstract class AbstractTrialSetting
    {
        public long Id { get; set; }
        public int TrialDays { get; set; }
    }

    public abstract class AbstractFlashScreens
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public string FileUrl { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate?.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate?.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string FileUrlStr => Configurations.ClientURL + FileUrl;
    }
}

