﻿using NewsPaperApp;
using NewsPaperApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace NewsPaperApp.Entities.Contract
{
    public abstract class AbstractUserNotifications
    {
        public long Id { get; set; }                                               
        public long UserId { get; set; }
        public string Name { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmail { get; set; }
        public string UserIds { get; set; }
        public string NotificationText { get; set; }
        public long IsRead { get; set; }
        public int Isent { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        
        

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

    }
}

