﻿using NewsPaperApp;
using NewsPaperApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace NewsPaperApp.Entities.Contract
{
    public abstract class AbstractPaymentPlans
    {
        public long Id { get; set; }
        public string PlanName { get; set; }
        public string Price { get; set; }
        public string ValidForDays { get; set; }
    }
}

