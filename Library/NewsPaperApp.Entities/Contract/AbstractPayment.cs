﻿using NewsPaperApp;
using NewsPaperApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace NewsPaperApp.Entities.Contract
{
    public abstract class AbstractPayment
    {
        public long Id { get; set; }
        public long UserId { get; set; }    
        public string Name { get; set; }
        public string FirstName { get; set; }
        public bool IsManual { get; set; }
        public string LastName { get; set; }
        public string TransactionId { get; set; }
        public int LanguageId { get; set; }
        public DateTime Time { get; set; }
        public string PaymentExpiryDays { get; set; }
        public DateTime PaymentExpiryDate { get; set; }
        [NotMapped]
        public string TimeStr => Time != null ? Time.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string PaymentExpiryDateStr => PaymentExpiryDate != null ? PaymentExpiryDate.ToString("dd-MMM-yyyy") : "-";
    }
}