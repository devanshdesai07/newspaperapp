﻿using NewsPaperApp;
using NewsPaperApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace NewsPaperApp.Entities.Contract
{
    public abstract class AbstractUsers
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public long IsActive { get; set; }
        public long PaymentPlanId { get; set; }
        public string PaymentPlanName { get; set; }
        public DateTime PaymentExpiryDate { get; set; }
        public string PaymentPlanExpiryDateFrom { get; set; }
        public string PaymentPlanExpiryDateTo { get; set; }
        public DateTime LastLogin { get; set; }
        public string DeviceTokens { get; set; }
        public string DeviceUDId { get; set; }
        public bool IsFiestTimeLogin { get; set; }
        public long OTP { get; set; }
        public long CountryId { get; set; }
        public long? LanguageId { get; set; }
        public long StateId { get; set; }
        public long CityId { get; set; }
        public string OS { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public bool IsAddedAdmin { get; set; }
        public string IOSDisplayMessage { get; set; }
        public string AndroidDisplayMessage { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string LastLoginStr => LastLogin != null ? LastLogin.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string PaymentDateStr => PaymentDate != null ? PaymentDate.ToString("yyyy-MM-dd") : "-";
        [NotMapped]
        public string PaymentExpiryDateStr => PaymentExpiryDate != null ? PaymentExpiryDate.ToString("yyyy-MM-dd") : "-";
    }

    public abstract class AbstractLanguages
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}

