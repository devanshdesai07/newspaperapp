﻿using NewsPaperApp;
using NewsPaperApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace NewsPaperApp.Entities.Contract
{
    public abstract class AbstractTransaction
    {
        public long Id { get; set; }
        public long User_Id { get; set; }                                
        public long Plan_Id { get; set; }                
        public string Transaction_Id { get; set; }
        public string Transaction_Type { get; set; }
        public long Transaction_Status { get; set; }
        public DateTime CreatedDate { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}

