﻿using NewsPaperApp;
using NewsPaperApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace NewsPaperApp.Entities.Contract
{
    public abstract class AbstractState
    {
        public long Id { get; set; }
        public long CountryId { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

    }
}

