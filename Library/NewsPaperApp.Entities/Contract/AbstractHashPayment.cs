﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsPaperApp.Entities.Contract
{
    public abstract class AbstractHashPayment
    {

        public string address_line_1 { get; set; }
        public string address_line_2 { get; set; }
        public string amount { get; set; }
        public string api_key { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public string email { get; set; }
        public string mode { get; set; }
        public string name { get; set; }
        public string order_id { get; set; }
        public string phone { get; set; }
        public string return_url { get; set; }
        public string state { get; set; }
        public string udf1 { get; set; }
        public string udf2 { get; set; }
        public string udf3 { get; set; }
        public string udf4 { get; set; }
        public string udf5 { get; set; }
        public string zip_code { get; set; }
        public string salt { get; set; }
    }
}
