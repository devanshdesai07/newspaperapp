﻿using NewsPaperApp.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsPaperApp.Entities.V1
{
    public class UploadedFiles : AbstractUploadedFiles
    {
    } 
    public class MasterLanguage : AbstractMasterLanguage
    {
    }

    public class TrialSetting : AbstractTrialSetting { }
    public class FlashScreens : AbstractFlashScreens { }
}
