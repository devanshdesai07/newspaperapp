﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;

namespace Send_News_Paper_Notification
{
    class Program
    {
        private static object redirectJson;

        public static string ConnStr = "Data Source=rushkar-db-z.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=NewsPaperApp;Persist Security Info=True;User ID=sa;Password=*aA123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";

        static void Main(string[] args)
        {
            while (true)
            {
                PaymentExpiryDate();
                SendNewsPaperNotification();
                SendOnDemandNotification();
            }
        }

        public static void PaymentExpiryDate()
        {
            SqlConnection sqlConn = null;
            SqlDataReader sqlDr = null;

            try
            {
                // Open a connection to SQL Server
                sqlConn = new SqlConnection(ConnStr);
                sqlConn.Open();

                SqlCommand sqlCmd = new SqlCommand("[dbo].[PaymentExpiryDate_Upsert]", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlDr = sqlCmd.ExecuteReader();
                sqlConn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void SendNewsPaperNotification()
        {
            List<string> DeviceT = new List<string>();
            Console.WriteLine("Getting Connection ...");

            //create instanace of database connection
            SqlConnection conn = new SqlConnection(ConnStr);
            try
            {
                DateTime now = DateTime.Now;
                string q = "select * from [dbo].[UploadedFiles] where FireDate <= Format(cast(dateadd(minute, 330, getUTCdate()) as datetime),'yyyy-MM-dd HH:mm:ss','en-us') AND IsFired != 1 AND DeletedBy=0";

                SqlDataAdapter ad = new SqlDataAdapter(q, ConnStr);
                DataTable dt = new DataTable();
                ad.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        long NewsPaperId = Convert.ToInt64(row["Id"]);

                        long LanguageId = Convert.ToInt64(row["FileLanguageId"]);

                        string getDT = "select DeviceTokens from [dbo].[Users] WHERE LanguageId = " + LanguageId + " AND DeviceTokens IS NOT NULL AND DeviceTokens != '' AND (DeletedBy=0 OR DeletedBy IS NULL) order by Id desc";
                        SqlDataAdapter DT = new SqlDataAdapter(getDT, ConnStr);
                        DataTable DEDeviceToken = new DataTable();
                        DT.Fill(DEDeviceToken);
                        DeviceT = new List<string>();

                        SqlConnection Conn = new SqlConnection(ConnStr);
                        Conn.Open();
                        string updateIsSent = "UPDATE UploadedFiles SET IsFired = 1 where Id = " + Convert.ToInt64(row["Id"]);
                        SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);
                        UpdateDENotificationcmd.ExecuteNonQuery();
                        Conn.Close();

                        Console.WriteLine(DEDeviceToken.Rows.Count);
                        if (DEDeviceToken.Rows.Count > 0)
                        {
                            for (int i = 0; i < DEDeviceToken.Rows.Count; i++)
                            {
                                DeviceT = new List<string>();
                                DeviceT.Add(Convert.ToString(DEDeviceToken.Rows[i]["DeviceTokens"]));

                                SendNotification(DeviceT, Convert.ToInt64(row["Id"]));

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        public static string SendNotification(List<string> DeviceT, long NewsPaperId)
        {
            string response = string.Empty;
            string serverKey = "AAAA4Dq4gQM:APA91bG57-L2ntII-gNUTWcMLZymfUEwJbgmjYkFOMHjCZzUCdiJYrtzVeCgYu-WqOLPymPWkJ44xII9Y3Noj0_cFqYCXdDOLBD8ALB_ijq6BVGHSk01VOrhtGd6xxawL4RxmghTTjPF"; // Something very long
                                                                                                                                                                                           //string senderId = "439168650109";
            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            string Description = "Newspaper available for : " + DateTime.UtcNow.AddHours(5).AddMinutes(30).ToString("dd-MMM-yyyy");

            var data = new
            {
                registration_ids = DeviceT,
                notification = new
                {
                    body = Description,
                    title = "Greetings from Capital World !",
                    sound = "Enabled"
                },
                data = new
                {
                    payload = Description
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentLength = byteArray.Length;
            string q = "";
            string S = "";
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            string sResponseFromServer = tReader.ReadToEnd();
                        }
                    }
                }
            }

            return "";
        }

        public static void SendOnDemandNotification()
        {
            List<string> DeviceT = new List<string>();
            Console.WriteLine("Getting Connection ...");

            //create instanace of database connection
            SqlConnection conn = new SqlConnection(ConnStr);
            try
            {

                string q = "select * from [dbo].[UserNotifications] where IsSent = 2";
                SqlDataAdapter ad = new SqlDataAdapter(q, ConnStr);
                DataTable dt = new DataTable();
                ad.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["UserId"].ToString() != "")
                        {
                            string getDT = "select DeviceTokens from [dbo].[Users] where Id = " + row["UserId"].ToString() + " AND DeviceTokens IS NOT NULL AND DeviceTokens != '' order by Id desc";
                            SqlDataAdapter DT = new SqlDataAdapter(getDT, ConnStr);
                            DataTable DEDeviceToken = new DataTable();
                            DT.Fill(DEDeviceToken);
                            DeviceT = new List<string>();

                            if (DEDeviceToken.Rows.Count > 0)
                            {
                                DeviceT = DEDeviceToken.AsEnumerable()
                                          .Select(r => r.Field<string>("DeviceTokens"))
                                          .ToList();

                                if (DeviceT.Count > 0)
                                {
                                    SendNotification(DeviceT, row["NotificationText"].ToString(), row["Id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        public static string SendNotification(List<string> DeviceT, string Description, string Id)
        {

            string response = string.Empty;
            string serverKey = "AAAA4Dq4gQM:APA91bG57-L2ntII-gNUTWcMLZymfUEwJbgmjYkFOMHjCZzUCdiJYrtzVeCgYu-WqOLPymPWkJ44xII9Y3Noj0_cFqYCXdDOLBD8ALB_ijq6BVGHSk01VOrhtGd6xxawL4RxmghTTjPF"; // Something very long
                                                                                                                                                                                           //string senderId = "439168650109";
            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            var data = new
            {
                registration_ids = DeviceT,
                notification = new
                {
                    body = Description,
                    title = "Greetings from Capital World !",
                    sound = "Enabled"
                },
                data = new
                {
                    payload = Description
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentLength = byteArray.Length;
            string q = "";
            string S = "";
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            string sResponseFromServer = tReader.ReadToEnd();

                            var ConnStr = "Data Source=rushkar-db-z.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=NewsPaperApp;Persist Security Info=True;User ID=sa;Password=*aA123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
                            SqlConnection Conn = new SqlConnection(ConnStr);
                            Conn.Open();
                            string updateIsSent = "UPDATE UserNotifications SET IsSent = 1 where Id = " + Convert.ToInt64(Id);
                            SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);
                            UpdateDENotificationcmd.ExecuteNonQuery();
                            Conn.Close();
                            Console.WriteLine("Send successful!");
                        }
                    }
                }
            }

            return "";
        }
    }
}
