﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Data;
using System.Web.Script.Serialization;

namespace UpdatePaymentJobConsole
{
    class Program
    {
        static void Main(string[] args)
        {


            var ConnStr = "Data Source=rushkar-db-z.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=NewsPaperApp;Persist Security Info=True;User ID=sa;Password=*aA123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
            SqlConnection Conn = new SqlConnection(ConnStr);

            try
            {
                while (true)
                {
                    List<string> DeviceT = new List<string>();
                    // string date_from = DateTime.UtcNow.AddHours(5).AddMinutes(25).ToString("yyyy-MM-dd HH:mm:ss");
                    string date_from = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                    string date_to = DateTime.UtcNow.AddHours(5).AddMinutes(30).ToString("yyyy-MM-dd HH:mm:ss");
                    
                    string[] hash_columns = {
                        "api_key",
                        "date_from",
                        "date_to",
                        "per_page"
                    };

                    string apikey = "40eebc0f-a80d-4e8f-8dc5-5f34b09e6526";
                    string salt = "7b57e1f3bada8dbbf444d231476223b5680977d8";

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://biz.aggrepaypayments.com/v2/paymentstatus");
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(new
                        {
                            api_key = apikey,
                            date_from = date_from,
                            date_to = date_to,
                            page_number = "1",
                            per_page = "50",
                            hash = gethashorderId(apikey, salt, date_from, date_to, "50", "1")
                        });
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();

                        if (result.ToString().ToLower().Trim().Contains("success"))
                        {
                            AgreePayResponse Agreeresponse = Newtonsoft.Json.JsonConvert.DeserializeObject<AgreePayResponse>(result.Replace("\n", ""));

                            Console.WriteLine("Total AgreePayCount : " + Agreeresponse.data.Count);

                            if (Agreeresponse.data.Count > 0)
                            {
                                for (int count = 0; count < Agreeresponse.data.Count; count++)
                                {
                                    if (Agreeresponse.data[count].customer_phone != null)
                                    {
                                        var ExistingUser = "SELECT * FROM Users WHERE PhoneNumber = '" + Convert.ToString(Agreeresponse.data[count].customer_phone) + "' AND DeletedBy = 0 AND PaymentExpiryDate IS NULL";
                                        SqlDataAdapter ad = new SqlDataAdapter(ExistingUser, ConnStr);
                                        DataTable dt = new DataTable();
                                        ad.Fill(dt);
                                        DeviceT = new List<string>();

                                        if (dt.Rows.Count > 0)
                                        {
                                            foreach (DataRow row in dt.Rows)
                                            {
                                                string updateUser = "Update Users SET PaymentExpiryDate = DATEADD(dd, 365, GETUTCDATE()), PaymentDate = GETUTCDATE(), IsFiestTimeLogin = 0, UpdatedDate = GETUTCDATE() where PaymentExpiryDate IS NULL AND PhoneNumber = '" + Convert.ToString(Agreeresponse.data[count].customer_phone) + "' AND DeletedBy = 0"; ;
                                                SqlCommand UpUser = new SqlCommand(updateUser, Conn);
                                                Conn.Open();
                                                UpUser.ExecuteNonQuery();
                                                Conn.Close();

                                                for (int i = 0; i < dt.Rows.Count; i++)
                                                {
                                                    DeviceT = new List<string>();
                                                    DeviceT.Add(Convert.ToString(dt.Rows[i]["DeviceTokens"]));

                                                    SendNotification(DeviceT);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            
                        }
                        else
                        {
                            Console.WriteLine("Failure from response");
                        }
                    }

                    System.Threading.Thread.Sleep(120000);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Threw an exception of " + ex.Message);
                Conn.Close();
            }
        }

        public static string gethashorderId(string apikey, string salt, string date_from, string date_to, string per_page, string page_number)
        {

            string checksumString;
            checksumString = salt;

            checksumString += "|" + apikey + "|" + date_from + "|" + date_to + "|" + page_number + "|" + per_page;

            string result = Generatehash512(checksumString);
            return result;
        }

        public static string Generatehash512(string text)
        {

            byte[] message = System.Text.Encoding.UTF8.GetBytes(text);

            System.Text.UnicodeEncoding UE = new System.Text.UnicodeEncoding();
            byte[] hashValue;
            SHA512Managed hashString = new SHA512Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex.ToUpper();

        }

        public static string SendNotification(List<string> DeviceT)
        {
            try
            {
                string response = string.Empty;
                string serverKey = "AAAA4Dq4gQM:APA91bG57-L2ntII-gNUTWcMLZymfUEwJbgmjYkFOMHjCZzUCdiJYrtzVeCgYu-WqOLPymPWkJ44xII9Y3Noj0_cFqYCXdDOLBD8ALB_ijq6BVGHSk01VOrhtGd6xxawL4RxmghTTjPF"; // Something very long
                                                                                                                                                                                               //string senderId = "439168650109";
                HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                string Description = "Thanks for the payment. Now you can access the application.";

                var data = new
                {
                    registration_ids = DeviceT,
                    notification = new
                    {
                        body = Description,
                        title = "Greetings from Capital World !",
                        sound = "Enabled"
                    },
                    data = new
                    {
                        payload = Description
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
                //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                string q = "";
                string S = "";
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                string sResponseFromServer = tReader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return "";
        }
    }
}


// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
public class Datum
{
    public string transaction_id { get; set; }
    public string bank_code { get; set; }
    public string payment_mode { get; set; }
    public string payment_channel { get; set; }
    public string payment_datetime { get; set; }
    public int response_code { get; set; }
    public string response_message { get; set; }
    public string authorization_staus { get; set; }
    public string order_id { get; set; }
    public string amount { get; set; }
    public string amount_orig { get; set; }
    public double tdr_amount { get; set; }
    public double tax_on_tdr_amount { get; set; }
    public string description { get; set; }
    public object error_desc { get; set; }
    public string customer_phone { get; set; }
    public string customer_name { get; set; }
    public string customer_email { get; set; }
    public string currency { get; set; }
    public string cardmasked { get; set; }
    public object udf1 { get; set; }
    public object udf2 { get; set; }
    public object udf3 { get; set; }
    public object udf4 { get; set; }
    public object udf5 { get; set; }
    public string bank_ref_number { get; set; }
}

public class Page
{
    public int total { get; set; }
    public int per_page { get; set; }
    public int current_page { get; set; }
    public int last_page { get; set; }
    public int from { get; set; }
    public int to { get; set; }
}

public class AgreePayResponse
{
    public List<Datum> data { get; set; }
    public Page page { get; set; }
    public string hash { get; set; }
}