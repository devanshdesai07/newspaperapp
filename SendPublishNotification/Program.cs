﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace SendPublishNotification
{
    class Program
    {
        public static string ConnStr = "Data Source=rushkar-db-z.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=NewsPaperApp;Persist Security Info=True;User ID=sa;Password=*aA123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";

        static void Main(string[] args)
        {

            List<string> DeviceT = new List<string>();
            Console.WriteLine("Getting Connection ...");

            //create instanace of database connection
            SqlConnection conn = new SqlConnection(ConnStr);
            try
            {
                DateTime now = DateTime.Now;
                string q = "select * from [dbo].[UploadedFiles] where FireDate = GETDATE()";
                
                SqlDataAdapter ad = new SqlDataAdapter(q, ConnStr);
                DataTable dt = new DataTable();
                ad.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        
                        string getDT = "select DeviceTokens from [dbo].[Users] DeviceTokens IS NOT NULL AND DeviceTokens != '' order by Id desc";
                        SqlDataAdapter DT = new SqlDataAdapter(getDT, ConnStr);
                        DataTable DEDeviceToken = new DataTable();
                        DT.Fill(DEDeviceToken);
                        DeviceT = new List<string>();

                        if (DEDeviceToken.Rows.Count > 0)
                        {
                            DeviceT = DEDeviceToken.AsEnumerable()
                                        .Select(r => r.Field<string>("DeviceTokens"))
                                        .ToList();

                            if (DeviceT.Count > 0)
                            {
                                SendNotification(DeviceT);
                            }
                        }
                        
                    }
                }
            } 
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            Console.Read();
        }

        public static string SendNotification(List<string> DeviceT)
        {

            string response = string.Empty;
            string serverKey = "AAAA6tsjjhM:APA91bEbAdQr_VHZ5pHjtTUhspCHe_bN2GTQfYXbgu4skFl7n2_jGcAhw2A8Rox1qAf5UyFW7eSpVmREt-9VcWe_MENAnNSU9ZUDFw7WUzxFt06kcsB2p0mtor1mAp4IRQSspn0QnLNK"; // Something very long
                                                                                                                                                                                           //string senderId = "439168650109";
            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            var data = new
            {
                registration_ids = DeviceT,
                //to = deviceId,
                data = new
                {
                    body = "Today's News Paper Is Available",//"Your job Status has been changed",                    
                    title = "",//"Fitting job has been completed.",
                    sound = "default",
                    mutable_contenct = true,
                    badge = 4,
                    data = new { }
                },
                mutable_contenct = true
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentLength = byteArray.Length;
            string q = "";
            string S = "";
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            string sResponseFromServer = tReader.ReadToEnd();
                            Console.WriteLine("Send successful!");
                        }
                    }
                }
            }

            return "";
        }
    }
}
