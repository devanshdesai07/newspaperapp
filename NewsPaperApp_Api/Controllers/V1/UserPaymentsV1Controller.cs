﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserPaymentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserPaymentsServices abstractUserPaymentsServices;
        #endregion

        #region Cnstr
        public UserPaymentsV1Controller(AbstractUserPaymentsServices abstractUserPaymentsServices)
        {
            this.abstractUserPaymentsServices = abstractUserPaymentsServices;
        }
        #endregion

        // UserPayments_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserPayments_Upsert")]
        public async Task<IHttpActionResult> UserPayments_Upsert(UserPayments UserPayments)
        {
            var quote = abstractUserPaymentsServices.UserPayments_Upsert(UserPayments);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // UserPayments_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserPayments_All")]
        public async Task<IHttpActionResult> UserPayments_ByUserId(PageParam pageParam, long PaymentPlanId = 0)
        {
            var quote = abstractUserPaymentsServices.UserPayments_ByUserId(pageParam, PaymentPlanId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}