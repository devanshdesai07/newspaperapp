﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CountryV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCountryServices abstractCountryServices;
        #endregion

        #region Cnstr
        public CountryV1Controller(AbstractCountryServices abstractCountryServices)
        {
            this.abstractCountryServices = abstractCountryServices;
        }
        #endregion

       
        // Admin_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Country_All")]
        public async Task<IHttpActionResult> Country_All(PageParam pageParam, string search = "")
        {
            var quote = abstractCountryServices.Country_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        
    }
}