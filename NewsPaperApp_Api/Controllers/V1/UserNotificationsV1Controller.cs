﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserNotificationsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserNotificationsServices abstractUserNotificationsServices;
        #endregion

        #region Cnstr
        public UserNotificationsV1Controller(AbstractUserNotificationsServices abstractUserNotificationsServices)
        {
            this.abstractUserNotificationsServices = abstractUserNotificationsServices;
        }
        #endregion

        // UserNotifications_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotifications_Upsert")]
        public async Task<IHttpActionResult> UserNotifications_Upsert(UserNotifications userNotifications)
        {
            var quote = abstractUserNotificationsServices.UserNotifications_Upsert(userNotifications);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // UserNotifications_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotifications_All")]
        public async Task<IHttpActionResult> UserNotifications_All(PageParam pageParam, string search = "", long UserId = 0, string FromDate = "", string ToDate = "", long IsRead = 0)
        {
            var quote = abstractUserNotificationsServices.UserNotifications_All(pageParam, search,UserId, FromDate, ToDate, IsRead);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserNotifications_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotifications_ById")]
        public async Task<IHttpActionResult> UserNotifications_ById(long Id)
        {
            var quote = abstractUserNotificationsServices.UserNotifications_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserNotifications_ReadUnReadAPI
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotifications_ReadUnRead")]
        public async Task<IHttpActionResult> UserNotifications_ReadUnRead(long Id)
        {
            var quote = abstractUserNotificationsServices.UserNotifications_ReadUnRead(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserNotifications_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotifications_ByUserId")]
        public async Task<IHttpActionResult> UserNotifications_ByUserId(PageParam pageParam, long UserId = 0)
        {
            var quote = abstractUserNotificationsServices.UserNotifications_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserNotifications_ByUserIdReadUnRead API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotifications_ByUserIdReadUnRead")]
        public async Task<IHttpActionResult> UserNotifications_ByUserIdReadUnRead(PageParam pageParam, long UserId = 0)
        {
            var quote = abstractUserNotificationsServices.UserNotifications_ByUserIdReadUnRead(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}