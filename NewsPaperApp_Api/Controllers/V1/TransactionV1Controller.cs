﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TransactionV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTransactionServices abstractTransactionServices;
        #endregion

        #region Cnstr
        public TransactionV1Controller(AbstractTransactionServices abstractTransactionServices)
        {
            this.abstractTransactionServices = abstractTransactionServices;
        }
        #endregion

        // Transaction_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Transaction_Insert")]
        public async Task<IHttpActionResult> Transaction_Insert(Transaction Transaction)
        {
            var quote = abstractTransactionServices.Transaction_Insert(Transaction);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Transaction_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Transaction_ByUserId")]
        public async Task<IHttpActionResult> Transaction_ByUserId(PageParam pageParam, string search = "", long User_Id = 0, long Plan_Id = 0, long Transaction_Status = 0, string FromCreatedDate = "", string ToCreatedDate = "")
        {
            var quote = abstractTransactionServices.Transaction_ByUserId(pageParam, search, User_Id, Plan_Id, Transaction_Status, FromCreatedDate, ToCreatedDate);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}