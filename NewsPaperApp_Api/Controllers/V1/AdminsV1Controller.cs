﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdminsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAdminsServices abstractAdminsServices;
        #endregion

        #region Cnstr
        public AdminsV1Controller(AbstractAdminsServices abstractAdminsServices)
        {
            this.abstractAdminsServices = abstractAdminsServices;
        }
        #endregion

        // Admin_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_Upsert")]
        public async Task<IHttpActionResult> Admin_Upsert(Admins admins)
        {
            var quote = abstractAdminsServices.Admin_Upsert(admins);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Admin_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_All")]
        public async Task<IHttpActionResult> Admin_All(PageParam pageParam, string search = "")
        {
            var quote = abstractAdminsServices.Admin_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Admin_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_ById")]
        public async Task<IHttpActionResult> Admin_ById(long Id)
        {
            var quote = abstractAdminsServices.Admin_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Admin_ActInact Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_ActInact")]
        public async Task<IHttpActionResult> Admin_ActInact(long Id)
        {
            var quote = abstractAdminsServices.Admin_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Admin_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_Delete")]
        public async Task<IHttpActionResult> Admin_Delete(long Id)
        {
            var quote = abstractAdminsServices.Admin_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Admin_Login API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_Login")]
        public async Task<IHttpActionResult> Admin_Login(string Username, string Password)
        {
            var quote = abstractAdminsServices.Admin_Login(Username, Password);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Admin_Logout API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_Logout")]
        public async Task<IHttpActionResult> Admin_Logout(long Id)
        {
            var quote = abstractAdminsServices.Admin_Logout(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        // Admin_ChangePassword API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_ChangePassword")]
        public async Task<IHttpActionResult> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            var quote = abstractAdminsServices.Admin_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}