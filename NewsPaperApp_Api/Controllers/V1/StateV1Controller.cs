﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StateV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractStateServices abstractStateServices;
        #endregion

        #region Cnstr
        public StateV1Controller(AbstractStateServices abstractStateServices)
        {
            this.abstractStateServices = abstractStateServices;
        }
        #endregion



        //State_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("State_ByCountryId")]
        public async Task<IHttpActionResult> State_ByCountryId(PageParam pageParam, long CountryId = 0)
        {
            var quote = abstractStateServices.State_ByCountryId(pageParam, CountryId);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}