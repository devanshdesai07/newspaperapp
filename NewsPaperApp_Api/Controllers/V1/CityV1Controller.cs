﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CityV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCityServices abstractCityServices;
        #endregion

        #region Cnstr
        public CityV1Controller(AbstractCityServices abstractCityServices)
        {
            this.abstractCityServices = abstractCityServices;
        }
        #endregion

        //City_ByStateId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("City_ByStateId")]
        public async Task<IHttpActionResult> City_ByStateId(PageParam pageParam, string CityName = "")
        {
            var quote = abstractCityServices.City_ByStateId(pageParam, CityName);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}