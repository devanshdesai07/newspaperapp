﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEPaymentApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using System.Security.Cryptography;
using System.Text;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Configuration;

namespace RealEPaymentApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PaymentV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPaymentServices abstractPaymentServices;
        #endregion

        #region Cnstr
        public PaymentV1Controller(AbstractPaymentServices abstractPaymentServices)
        {
            this.abstractPaymentServices = abstractPaymentServices;
        }
        #endregion

        // Payment_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_Upsert")]
        public async Task<IHttpActionResult> Payment_Upsert(Payment Payment)
        {
            var quote = abstractPaymentServices.Payment_Upsert(Payment);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        public string gethash(string apikey, string salt, string tranid)
        {

            string checksumString;
            checksumString = salt;

            checksumString += "|" + apikey + "|" + tranid;

            string result = Generatehash512(checksumString);
            return result;
        }

        // Android 

        // Payment_AndroidUpsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_AndroidUpsert")]
        public async Task<IHttpActionResult> Payment_AndroidUpsert(Payment Payment)
        {
            if (Payment.TransactionId != null && Payment.TransactionId != "" && Payment.LanguageId != 2)
            {
                string[] hash_columns = {
                "api_key",
                "transaction_id"
                };

                string apikey = ConfigurationManager.AppSettings["ApiKey"];
                string salt = ConfigurationManager.AppSettings["Salt"];

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["WebRequestLink"]);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        api_key = apikey,
                        transaction_id = Payment.TransactionId,
                        hash = getadnroidhash(apikey, salt, Payment.TransactionId)
                    });
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    if (!result.ToString().ToLower().Contains("success"))
                    {
                        return null;
                    }
                }
            }

            var quote = abstractPaymentServices.Payment_AndroidUpsert(Payment);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        public string getadnroidhash(string apikey, string salt, string tranid)
        {

            string checksumString;
            checksumString = salt;

            checksumString += "|" + apikey + "|" + tranid;

            string result = Generatehash512(checksumString);
            return result;
        }

        // Payment_All API byplanid
        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_All")]
        public async Task<IHttpActionResult> Payment_All(PageParam pageParam, string search = "", long UserId = 0,
            string PaymentDateFrom = "", string PaymentDateTo = "", string PaymentExpiryDateFrom = "",
            string PaymentExpiryDateTo = "")
        {
            var quote = abstractPaymentServices.Payment_All(pageParam, search, UserId, PaymentDateFrom, PaymentDateTo,
                PaymentExpiryDateFrom, PaymentExpiryDateTo);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Payment_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_ById")]
        public async Task<IHttpActionResult> Payment_ById(long Id)
        {
            var quote = abstractPaymentServices.Payment_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_ByUserId")]
        public async Task<IHttpActionResult> Payment_ByUserId(PageParam pageParam, long UserId = 0)
        {
            var quote = abstractPaymentServices.Payment_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("Payment_ByTransactionId")]
        //public async Task<IHttpActionResult> Payment_ByTransactionId(PageParam pageParam, string TransactionId = "")
        //{
        //    var quote = abstractPaymentServices.Payment_ByTransactionId(pageParam, TransactionId);
        //    return this.Content((HttpStatusCode)200, quote);
        //}
        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_Hash")]
        public async Task<IHttpActionResult> Payment_Hash(HashPayment payment)
        {
            string quote = gethash(payment);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("paymentcallback")]
        public string PaymentCallBack(AgreePayResponse agreePayResponse)
        {
            string text = "";
            text = JsonConvert.SerializeObject(agreePayResponse);

            var ConnStr = "Data Source=database-zeki.cgyhfqhnqm3z.eu-west-2.rds.amazonaws.com;Initial Catalog=NewsPaperApp;Persist Security Info=True;User ID=sa;Password=*aA123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
            SqlConnection Conn = new SqlConnection(ConnStr);
            Conn.Open();
            
            string updateIsSent = "INSERT INTO ttt (text) VALUES ('" + text + "')";
            SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);
            UpdateDENotificationcmd.ExecuteNonQuery();

            if (agreePayResponse.response_message.ToLower() == "success")
            {
                string updateUser = "Update Users SET PaymentExpiryDate = DATEADD(dd, 365, GETDATE()), PaymentDate = GETDATE(), IsFiestTimeLogin = 0, UpdatedDate = GETDATE() where PhoneNumber = " + Convert.ToString(agreePayResponse.phone);
                SqlCommand UpUser = new SqlCommand(updateUser, Conn);
                UpUser.ExecuteNonQuery();
            }
            Conn.Close();
            return "";
        }


        [System.Web.Http.HttpGet]
        [InheritedRoute("GettransactionbyorderId")]
        public string GettransactionbyorderId(string orderId)
        {
            //string text = "";
            //text = JsonConvert.SerializeObject(responsePayment);
            //return text;

            if (orderId != null && orderId != "")
            {
                string[] hash_columns = {
                "api_key",
                "order_id"
                };

                string apikey = ConfigurationManager.AppSettings["ApiKey"];
                string salt = ConfigurationManager.AppSettings["Salt"];


                var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["WebRequestLink"]);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        api_key = apikey,
                        order_id = orderId,
                        hash = gethashorderId(apikey, salt, orderId)
                    });
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    //AGpayResposne AGPayresult = JsonConvert.DeserializeObject<AGpayResposne>(result);

                    if (!result.ToString().ToLower().Contains("success"))
                    {
                        return "";
                    }
                }
            }

            return "";
        }

        public string gethashorderId(string apikey, string salt, string orderid)
        {

            string checksumString;
            checksumString = salt;

            checksumString += "|" + apikey + "|" + orderid;

            string result = Generatehash512(checksumString);
            return result;
        }

        public string gethash(HashPayment payment)
        {

            string checksumString = Configurations.Hashsalt;

            if (payment.address_line_1 != null && payment.address_line_1 != "")
                checksumString += "|" + payment.address_line_1;

            if (payment.address_line_2 != null && payment.address_line_2 != "")
                checksumString += "|" + payment.address_line_2;

            if (payment.amount != null && payment.amount != "")
                checksumString += "|" + payment.amount;


            checksumString += "|" + Configurations.Hashapi_key;


            if (payment.city != null && payment.city != "")
                checksumString += "|" + payment.city;


            checksumString += "|" + Configurations.Hashcountry + "|" + Configurations.Hashcurrency;


            if (payment.description != null && payment.description != "")
                checksumString += "|" + payment.description;


            if (payment.email != null && payment.email != "")
                checksumString += "|" + payment.email;


            if (payment.mode != null && payment.mode != "")
                checksumString += "|" + payment.mode;


            if (payment.name != null && payment.name != "")
                checksumString += "|" + payment.name;

            if (payment.order_id != null && payment.order_id != "")
                checksumString += "|" + payment.order_id;


            if (payment.phone != null && payment.phone != "")
                checksumString += "|" + payment.phone;


            if (payment.return_url != null && payment.return_url != "")
                checksumString += "|" + payment.return_url;


            if (payment.state != null && payment.state != "")
                checksumString += "|" + payment.state;


            if (payment.udf1 != null && payment.udf1 != "")
                checksumString += "|" + payment.udf1;

            if (payment.udf2 != null && payment.udf2 != "")
                checksumString += "|" + payment.udf2;

            if (payment.udf3 != null && payment.udf3 != "")
                checksumString += "|" + payment.udf3;

            if (payment.udf4 != null && payment.udf4 != "")
                checksumString += "|" + payment.udf4;

            if (payment.udf5 != null && payment.udf5 != "")
                checksumString += "|" + payment.udf5;

            if (payment.zip_code != null && payment.zip_code != "")
                checksumString += "|" + payment.zip_code;


            string result = Generatehash512(checksumString);
            return result;
        }
        public string Generatehash512(string text)
        {

            byte[] message = System.Text.Encoding.UTF8.GetBytes(text);

            System.Text.UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            SHA512Managed hashString = new SHA512Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex.ToUpper();

        }
    }
}

// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
public class AgreePayResponse
{
    public string order_id { get; set; }
    public string amount { get; set; }
    public string currency { get; set; }
    public string description { get; set; }
    public string name { get; set; }
    public string email { get; set; }
    public string phone { get; set; }
    public string address_line_1 { get; set; }
    public string address_line_2 { get; set; }
    public string city { get; set; }
    public string state { get; set; }
    public string country { get; set; }
    public string zip_code { get; set; }
    public string udf1 { get; set; }
    public string udf2 { get; set; }
    public string udf3 { get; set; }
    public string udf4 { get; set; }
    public string udf5 { get; set; }
    public string transaction_id { get; set; }
    public string payment_mode { get; set; }
    public string payment_channel { get; set; }
    public string payment_datetime { get; set; }
    public int response_code { get; set; }
    public string response_message { get; set; }
    public string error_desc { get; set; }
    public object cardmasked { get; set; }
    public string hash { get; set; }
}

public class RemotePost
{
    private System.Collections.Specialized.NameValueCollection Inputs = new System.Collections.Specialized.NameValueCollection();


    public string Url = "";
    public string Method = "post";
    public string FormName = "form1";

    public void Add(string name, string value)
    {
        Inputs.Add(name, value);
    }

    public void Post()
    {
        System.Web.HttpContext.Current.Response.Clear();

        System.Web.HttpContext.Current.Response.Write("<html><head>");

        System.Web.HttpContext.Current.Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", FormName));
        System.Web.HttpContext.Current.Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, Url));
        for (int i = 0; i < Inputs.Keys.Count; i++)
        {
            System.Web.HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", Inputs.Keys[i], Inputs[Inputs.Keys[i]]));
        }
        System.Web.HttpContext.Current.Response.Write("</form>");
        System.Web.HttpContext.Current.Response.Write("</body></html>");

        System.Web.HttpContext.Current.Response.End();
    }
}