﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserFilesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserFilesServices abstractUserFilesServices;
        #endregion

        #region Cnstr
        public UserFilesV1Controller(AbstractUserFilesServices abstractUserFilesServices)
        {
            this.abstractUserFilesServices = abstractUserFilesServices;
        }
        #endregion

        // UserFiles_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserFiles_Upsert")]
        public async Task<IHttpActionResult> UserFiles_Upsert(UserFiles userFiles)
        {
            var quote = abstractUserFilesServices.UserFiles_Upsert(userFiles);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserFiles_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserFiles_ById")]
        public async Task<IHttpActionResult> UserFiles_ById(long Id)
        {
            var quote = abstractUserFilesServices.UserFiles_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        //UserFiles_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserFiles_Delete")]
        public async Task<IHttpActionResult> UserFiles_Delete(long Id)
        {
            var quote = abstractUserFilesServices.UserFiles_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //UserFiles_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserFiles_ByUserId")]
        public async Task<IHttpActionResult> UserFiles_ByUserId(PageParam pageParam, string search = "", long UserId = 0,string FileName = "",string FromUploadedDate = "",string ToUploadedDate = "",long UploadedByUsername = 0)
        {
            AbstractUserFiles userFiles = new UserFiles();
            userFiles.UserId = UserId;
            userFiles.FileName = FileName;
            userFiles.FromUploadedDate = FromUploadedDate;
            userFiles.ToUploadedDate = ToUploadedDate;
            userFiles.UploadedByUsername = UploadedByUsername;
            
            var quote = abstractUserFilesServices.UserFiles_ByUserId(pageParam, search, userFiles);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}