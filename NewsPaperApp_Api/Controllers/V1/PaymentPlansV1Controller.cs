﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PaymentPlansV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPaymentPlansServices abstractPaymentPlansServices;
        #endregion

        #region Cnstr
        public PaymentPlansV1Controller(AbstractPaymentPlansServices abstractPaymentPlansServices)
        {
            this.abstractPaymentPlansServices = abstractPaymentPlansServices;
        }
        #endregion

        // PaymentPlans_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("PaymentPlans_Upsert")]
        public async Task<IHttpActionResult> PaymentPlans_Upsert(PaymentPlans paymentPlans)
        {
            var quote = abstractPaymentPlansServices.PaymentPlans_Upsert(paymentPlans);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // PaymentPlans_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("PaymentPlans_All")]
        public async Task<IHttpActionResult> PaymentPlans_All(PageParam pageParam, string search = "")
        {
            var quote = abstractPaymentPlansServices.PaymentPlans_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //PaymentPlans_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("PaymentPlans_ById")]
        public async Task<IHttpActionResult> PaymentPlans_ById(long Id)
        {
            var quote = abstractPaymentPlansServices.PaymentPlans_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}