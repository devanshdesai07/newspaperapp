﻿using NewsPaperApp.APICommon;
using NewsPaperAppApi.Models;
using NewsPaperApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsPaperApp.Common.Paging;
using NewsPaperApp.Entities.Contract;
using NewsPaperApp.Services.Contract;
using NewsPaperApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Azure.Management.Fluent;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UploadedFilesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUploadedFilesServices abstractUploadedFilesServices;
        #endregion

        #region Cnstr
        public UploadedFilesV1Controller(AbstractUploadedFilesServices abstractUploadedFilesServices)
        {
            this.abstractUploadedFilesServices = abstractUploadedFilesServices;
        }
        #endregion

        // UploadedFiles_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UploadedFiles_Upsert")]
        public async Task<IHttpActionResult> UploadedFiles_Upsert()
        {
            UploadedFiles uploadedFiles = new UploadedFiles();
            var httpRequest = HttpContext.Current.Request;
            uploadedFiles.Id = Convert.ToInt64(httpRequest.Params["Id"]);
            uploadedFiles.FileName = Convert.ToString(httpRequest.Params["FileName"]);
            uploadedFiles.Note = Convert.ToString(httpRequest.Params["Note"]);
            uploadedFiles.UploadedDate = Convert.ToDateTime(httpRequest.Params["UploadedDate"]);
            uploadedFiles.UploadedBy = Convert.ToInt64(httpRequest.Params["UploadedBy"]);
            uploadedFiles.FireDate = Convert.ToDateTime(httpRequest.Params["FireDate"]);
            uploadedFiles.CreatedBy = Convert.ToInt64(httpRequest.Params["CreatedBy"]);
            uploadedFiles.UpdatedBy = Convert.ToInt64(httpRequest.Params["UpdatedBy"]);
            uploadedFiles.FileLanguageId = Convert.ToInt64(httpRequest.Params["FileLanguageId"]);

            if (httpRequest.Files.Count > 0)
            {
                if (uploadedFiles.FileName != null)
                {
                    var myFile = httpRequest.Files[0];
                    string basePath = "UploadedFiles/" + DateTime.Now.ToString("ddMMyyyyhhmmsstt") + uploadedFiles.Id + "/";
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + ((uploadedFiles.FileLanguageId == 1) ? "_" + Path.GetFileName(myFile.FileName) : ".pdf");


                    uploadedFiles.FileName = fileName;

                    if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                    }
                    myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                    uploadedFiles.FileUrl = basePath + fileName;

                    var filePath = HttpContext.Current.Server.MapPath("~/" + basePath + fileName);

                    var connectionString = "DefaultEndpointsProtocol=https;AccountName=capitalworldnewspapernew;AccountKey=NlxvXo5bQbpLt01NtNYIk4IEgV9pda1o6KrGQbfi8GA6DkE0ECYxWuYQKe1UVSQwDFCr9fjMp2Bifu5c3IPEQQ==;EndpointSuffix=core.windows.net";

                    // intialize BobClient 
                    Azure.Storage.Blobs.BlobClient blobClient = new Azure.Storage.Blobs.BlobClient(
                        connectionString: connectionString,
                        blobContainerName: "newspaperpdf",
                        blobName: fileName
                        );

                    // upload the file
                    blobClient.Upload(filePath);
                }
            }

            var quote = abstractUploadedFilesServices.UploadedFiles_Upsert(uploadedFiles);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //TrialSetting_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("TrialSetting_ById")]
        public async Task<IHttpActionResult> TrialSetting_ById(long Id)
        {
            var quote = abstractUploadedFilesServices.TrialSetting_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //TrialSetting_UpdateTrialDays API
        [System.Web.Http.HttpPost]
        [InheritedRoute("TrialSetting_UpdateTrialDays")]
        public async Task<IHttpActionResult> TrialSetting_UpdateTrialDays(TrialSetting trialSetting)
        {
            try
            {
                var quote = abstractUploadedFilesServices.TrialSetting_UpdateTrialDays(trialSetting);
                return this.Content((HttpStatusCode)quote.Code, quote);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //UploadedFiles_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UploadedFiles_ById")]
        public async Task<IHttpActionResult> UploadedFiles_ById(long Id)
        {
            var quote = abstractUploadedFilesServices.UploadedFiles_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserFiles_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UploadedFiles_ByDate")]
        public async Task<IHttpActionResult> UploadedFiles_ByDate(DateTime ByDate, long UserId = 0)
        {
            var quote = abstractUploadedFilesServices.UploadedFiles_ByDate(ByDate, UserId);

            var containerName = "newspaperpdf";
            var con = "DefaultEndpointsProtocol=https;AccountName=capitalworldnewspapernew;AccountKey=NlxvXo5bQbpLt01NtNYIk4IEgV9pda1o6KrGQbfi8GA6DkE0ECYxWuYQKe1UVSQwDFCr9fjMp2Bifu5c3IPEQQ==;EndpointSuffix=core.windows.net";
            CloudStorageAccount account = CloudStorageAccount.Parse(con);

            var blobClient = account.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(containerName);
            var blob = container.GetBlockBlobReference(quote.Item.FileName);
            var sasToken = blob.GetSharedAccessSignature(new SharedAccessBlobPolicy()
            {
                Permissions = SharedAccessBlobPermissions.Read,
                SharedAccessExpiryTime = DateTime.UtcNow.AddSeconds(120),
            }, new SharedAccessBlobHeaders()
            {
                ContentDisposition = "attachment; filename=" + quote.Item.FileName
            });

            var blobUrl = string.Format("{0}{1}", blob.Uri.AbsoluteUri, sasToken);

            quote.Item.FileUrl = blobUrl;

            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UploadedFiles_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UploadedFiles_Delete")]
        public async Task<IHttpActionResult> UploadedFiles_Delete(long Id)
        {
            var quote = abstractUploadedFilesServices.UploadedFiles_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // UploadedFiles_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UploadedFiles_All")]
        public async Task<IHttpActionResult> UploadedFiles_All(PageParam pageParam, string search = "", string NewsPaperName = "", string FromNewsPaperDate = "", string ToNewsPaperDate = "", string FromPublishedDate = "", string ToPublishedDate = "", long UploadedByName = 0, long FileLanguageId = 0)
        {
            AbstractUploadedFiles uploadedFiles = new UploadedFiles();
            uploadedFiles.NewsPaperName = NewsPaperName;
            uploadedFiles.FromNewsPaperDate = FromNewsPaperDate;
            uploadedFiles.ToNewsPaperDate = ToNewsPaperDate;
            uploadedFiles.FromPublishedDate = FromPublishedDate;
            uploadedFiles.ToPublishedDate = ToPublishedDate;
            uploadedFiles.UploadedByName = UploadedByName;
            uploadedFiles.FileLanguageId = FileLanguageId;

            var quote = abstractUploadedFilesServices.UploadedFiles_All(pageParam, search, uploadedFiles);
            return this.Content((HttpStatusCode)200, quote);
        }

        // MasterLanguage_SelectAll API
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterLanguage_SelectAll")]
        public async Task<IHttpActionResult> MasterLanguage_SelectAll(PageParam pageParam, string search = "")
        {
            var quote = abstractUploadedFilesServices.MasterLanguage_SelectAll(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //FlashScreens_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("FlashScreens_All")]
        public async Task<IHttpActionResult> FlashScreens_All(PageParam pageParam, string search = "")
        {
            //string today = DateTime.UtcNow.AddHours(-48).AddMinutes(330).ToString("yyyy-mm-dd hh:mm:ss.fff");
            var quote = abstractUploadedFilesServices.FlashScreens_All(pageParam, search, "");
            return this.Content((HttpStatusCode)200, quote);
        }

        //FlashScreens_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("FlashScreens_Delete")]
        public async Task<IHttpActionResult> FlashScreens_Delete(long Id, long DeletedBy)
        {
            var quote = abstractUploadedFilesServices.FlashScreens_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //FlashScreens_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("FlashScreens_ById")]
        public async Task<IHttpActionResult> FlashScreens_ById(long Id)
        {
            var quote = abstractUploadedFilesServices.FlashScreens_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //FlashScreens_Insert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("FlashScreens_Insert")]
        public async Task<IHttpActionResult> FlashScreens_Insert()
        {
            FlashScreens flashScreens = new FlashScreens();
            var httpRequest = HttpContext.Current.Request;
            flashScreens.Id = Convert.ToInt64(httpRequest.Params["Id"]);
            flashScreens.Description = Convert.ToString(httpRequest.Params["Description"]);
            flashScreens.CreatedBy = Convert.ToInt64(httpRequest.Params["CreatedBy"]);
            flashScreens.UpdatedBy = Convert.ToInt64(httpRequest.Params["UpdatedBy"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "FlashScreen/" + flashScreens.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                flashScreens.FileUrl = basePath + fileName;
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
            }

            var quote = abstractUploadedFilesServices.FlashScreens_Insert(flashScreens);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}